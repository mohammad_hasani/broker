import os

CSRF_ENABLED = True
SECRET_KEY = 'SO_SECRET'
WTF_CSRF_SECRET_KEY = 'a random string'

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

UPLOAD_FOLDER = BASE_DIR + '/src/static/user_data/'

IMAGE_ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
VIDEO_ALLOWED_EXTENSIONS = set(['mp4', 'avi'])
AUDIO_ALLOWED_EXTENSIONS = set(['mp3', 'ogg'])
DOCUMENT_ALLOWED_EXTENSIONS = set(['pdf'])
ALLOWED_EXTENSIONS = set(
    IMAGE_ALLOWED_EXTENSIONS | VIDEO_ALLOWED_EXTENSIONS | AUDIO_ALLOWED_EXTENSIONS | DOCUMENT_ALLOWED_EXTENSIONS)


if os.environ.get('SERVER'):
    MONGODB_SETTINGS = {
        'db': 'securypto',
        'host': 'localhost',
        'username': 'pooya',
        'password': '6211724',
        'port': 27017
    }
else:
    MONGODB_SETTINGS = {
        'db': 'securypto',
        'host': 'localhost',
        'port': 27017
    }

CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

BABEL_DEFAULT_LOCALE = 'en'

DEBUG = True
SITE_BASE_URL = 'localhost:8080'
FORWARD_URL = 'http://78.47.177.44:8080'

MAIL_SERVER = 'smtp.googlemail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'pooya.kn.73@gmail.com'
MAIL_PASSWORD = 'weqproqzsexpwjpq'
MAIL_DEFAULT_SENDER = 'pooya.kn.73@gmail.com'

BP_TERMINAL_ID = 217
BP_USERNAME = 'user217'
BP_PASSWORD = '83642285'
BP_CALLBACK_URL = FORWARD_URL + '/payment/verify-payment/'

RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'
