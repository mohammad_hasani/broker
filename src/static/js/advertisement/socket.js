/**
 * Created by BSIM on 4/5/2018.
 */
$(document).ready(function () {
    $('input#amount').keyup(function () {

        if ($('#price_model').val() == 'CUSTOM_DOLLAR') {
            socket.emit('CUSTOM_DOLLAR_PRICE', {amount: $(this).val(), coin_symbol: $('#coin').val(), dollar_price:$('input#dollar_price').val() });
        }
        else if ($('#price_model').val() == 'PER_UNIT') {
            socket.emit('PER_UNIT_PRICE',  {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), per_unit_price:$('input#per_unit_price').val()});
        }

    });

    $('#dollar_price').keyup(function ()
    {
        if ($('#price_model').val() == 'CUSTOM_DOLLAR')
        {
            socket.emit('CUSTOM_DOLLAR_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), dollar_price:$('input#dollar_price').val() });
        }
    })

    $('#per_unit_price').keyup(function ()
    {
        if ($('#price_model').val() == 'PER_UNIT')
        {
            socket.emit('PER_UNIT_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), per_unit_price:$('input#per_unit_price').val() });
        }
    })



    $('#coin').change(function () {

        if ($('#price_model').val() == 'CUSTOM_DOLLAR') {
            socket.emit('CUSTOM_DOLLAR_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), dollar_price:$('input#dollar_price').val() });

        }
        else if ($('#price_model').val() == 'PER_UNIT') {+
             socket.emit('PER_UNIT_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), per_unit_price:$('input#per_unit_price').val() });
        }

    });

    $('#price_model').change(function () {

        if ($('#price_model').val() == 'CUSTOM_DOLLAR') {
            socket.emit('CUSTOM_DOLLAR_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), dollar_price:$('input#dollar_price').val() });

        }
        else if ($('#price_model').val() == 'PER_UNIT') {+
             socket.emit('PER_UNIT_PRICE', {amount: $('input#amount').val(), coin_symbol: $('#coin').val(), per_unit_price:$('input#per_unit_price').val() });
        }

    });


    socket.on('CUSTOM_DOLLAR_RESULT', function (result) {
        $('.calculated_number').text(result['price'])
    })

    socket.on('PER_UNIT_RESULT', function (result) {
        $('.calculated_number').text(result['price'])
    })


    if($('.wallet').css('display') == 'block')
    {
        socket.emit('GET_BALANCE',{coin:$('#coin').val() , wallet: $('#wallet').val() });

        $('#wallet').change(function () {
            socket.emit('GET_BALANCE',{coin:$('#coin').val() , wallet: $('#wallet').val() });

        })
    }

    //handle result of etherium and bitcoin wallet check
    socket.on('GET_ETH_BALANCE_RESULT', function (data) {
        $('div.balance').text('your balance :' + data['balance']);
    })


     socket.on('GET_ETH_BALANCE_ERROR', function (data) {
        $('div.balance').text( data['error']);
    })

    // for bitcoin addresses

    socket.on('GET_BTC_BALANCE_RESULT', function (data) {
        $('div.balance').text('your balance :' + data['balance']);
    })


     socket.on('GET_BTC_BALANCE_ERROR', function (data) {
        $('div.balance').text( data['error']);
    })



});
