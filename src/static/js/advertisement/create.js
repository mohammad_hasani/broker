/**
 * Created by BSIM on 4/5/2018.
 */
$(document).ready(function () {
    if($('#wallet_type').val() == 'EXCHANGE')
    {
        $('#wallet').css('display','none');
    }



    $('div.per_unit_price').css('display','none');

    $('div.dollar_price').css('display','none');

    $('#price_model').on('change', function () {
        if ($('#price_model').val() == 'CUSTOM_DOLLAR')
        {
            $('div.per_unit_price').css('display','none');
            $('div.dollar_price').css('display','block');
            $('#dollar_price').prop('readonly', false);
            $('#dollar_price').select();



        }
        else if($('#price_model').val() == 'PER_UNIT')
        {
            $('div.dollar_price').css('display','none');
            $('div.per_unit_price').css('display','block');
            $('#per_unit_price').select();

        }
    })


    $('#wallet_type').on('change',  function ()
    {
        if($('#wallet_type').val() == 'PERSONAL')
            {
                $('.wallet').css('display','block');
                $('#wallet').css('display','block');
            }
        else if ($('#wallet_type').val() == 'EXCHANGE')
            {
                $('.wallet').css('display','none');
                $('#wallet').css('display','none');
            }


    })
    // get balance of wallet


});