from functools import wraps
from flask_login import current_user
from flask import abort


def get_current_user_role():
    return current_user.role

def error_response():
    return abort(401, 'User doesnt have permission to view this page.')

def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if get_current_user_role() not in roles:
                return error_response()
            return f(*args, **kwargs)
        return wrapped
    return wrapper