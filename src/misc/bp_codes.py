from enum import Enum

class BpCodes(Enum):
    SUCCESSFUL = 0
    INVALID_CARD_NUMBER = 11
    INSUFFICIENT_FUNDS = 12
    INCORRECT_PASSWORD = 13
    EXCESSIVE_INCORRECT_PASSWORDS = 14
    INVALID_CARD = 15
    EXCESSIVE_WITHDRAWAL_TIMES = 16
    USER_QUIT_TRANSACTION = 17
    CARD_EXPIRED = 18
    EXCESSIVE_WITHDRAWAL_AMOUNT = 19
    INVALID_ISSUER = 111
    ISSUER_SWITCH_FAILURE = 112
    NO_ANSWER_FROM_ISSUER = 113
    TRANSACTION_PROHIBITED = 114
    INVALID_RECEIVER = 21
    SECURITY_ERROR = 23
    INVALID_RECEIVER_INFO = 24
    INVALID_AMOUNT = 25
    INVALID_RESPONSE = 31
    INVALID_DATA = 32
    INVALID_ACCOUNT = 33
    SERVER_ERROR = 34
    INVALID_DATE = 35
    DUPLICATE_REQUEST_CODE = 41
    SALE_TRANSACTION_NOT_FOUND = 42
    VERIFY_REQUEST_ALREADY_MADE = 43
    VERIFY_REQUEST_NOT_FOUND = 44
    SETTLED_TRANSACTION = 45
    UNSETTELED_TRANSACTION = 46
    SETTLE_TRANSACTION_NOT_FOUND = 47
    RESERVED_TRANSACTION = 48
    REFUND_TRANSACTION_NOT_FOUND = 49
    INVALID_BILL_CODE = 412
    INVALID_PAYMENT_CODE = 413
    INVALID_BILL_ISSUER = 414
    SESSION_TIMEOUT = 415
    DATA_WRITE_ERROR = 416
    INVALID_PAYER_CODE = 417
    COSTUMER_INFO_ERROR = 418
    EXCESSIVE_DATA_ENTRY = 419
    INVALID_IP = 421
    DUPLICATE_TRANSACTION = 51
    REFERENCE_TRANSACTION_NOT_FOUND = 54
    INVALID_TRANSACTION = 55
    DEPOSIT_ERROR = 61

    @staticmethod
    def get_message_for_res_code(code):
        code = int(code)
        if code == BpCodes.SUCCESSFUL.value:
            return 'عملیات موفقیت آمیز بود.'
        if code == BpCodes.CARD_EXPIRED.value:
            return 'کارت شما منقصی شده است.'
        if code == BpCodes.INSUFFICIENT_FUNDS.value:
            return 'موجودی کافی نیست.'
        if code == BpCodes.INCORRECT_PASSWORD.value:
            return 'رمز کارت اشتباه است.'
        if code == BpCodes.EXCESSIVE_INCORRECT_PASSWORDS.value:
            return 'تعداد عبور رمز اشتباه بیش از حد مجاز.'
        if code == BpCodes.INVALID_CARD.value:
            return 'کارت نامعتبر.'
        if code == BpCodes.EXCESSIVE_WITHDRAWAL_TIMES.value:
            return 'تعداد دفعات برداشت وجه بیش از حد مجاز.'
        if code == BpCodes.USER_QUIT_TRANSACTION.value:
            return 'منصرف شدن از انجام تراکنش.'
        if code == BpCodes.CARD_EXPIRED.value:
            return 'تاریخ انقضای کارت گذشته است.'
        if code == BpCodes.EXCESSIVE_WITHDRAWAL_AMOUNT.value:
            return 'برداشت وجه بیش از مقدار مجاز روزانه است.'
        if code == BpCodes.INVALID_ISSUER.value:
            return 'صادرکننده کارت نامعبتر است.'
        if code == BpCodes.ISSUER_SWITCH_FAILURE.value:
            return 'خطای سوییچ صادرکننده کارت'
        if code == BpCodes.NO_ANSWER_FROM_ISSUER.value:
            return 'پاسخی از صادرکننده کارت دریافت نشد.'
        if code == BpCodes.TRANSACTION_PROHIBITED.value:
            return 'دارنده کارت مجاز به انجام تراکنش نیست.'
        if code == BpCodes.INVALID_RECEIVER.value:
            return 'پذیرنده نامعتبر است.'
        if code == BpCodes.SECURITY_ERROR.value:
            return 'خطای امنیتی رخ داده است.'
        if code == BpCodes.INVALID_RECEIVER_INFO.value:
            return 'اطلاعات کاربری پذیرنده نامعتبر است.'
        if code == BpCodes.INVALID_AMOUNT.value:
            return 'مبلغ نامعتبر است.'
        if code == BpCodes.INVALID_RESPONSE.value:
            return 'پاسخ نامعتبر است.'
        if code == BpCodes.INVALID_DATA.value:
            return 'فرمت اطلاعات وارد شده صحیح نیست.'
        if code == BpCodes.INVALID_ACCOUNT.value:
            return 'حساب نامعتبر است.'
        if code == BpCodes.SERVER_ERROR.value:
            return 'خطای سیستمی.'
        if code == BpCodes.INVALID_DATE.value:
            return 'تاریخ نامعتبر است.'
        if code == BpCodes.DUPLICATE_REQUEST_CODE.value:
            return 'شماره درخواست تکراری است.'
        if code == BpCodes.SALE_TRANSACTION_NOT_FOUND.value:
            return 'تراکنش Sale یافت نشد.'
        if code == BpCodes.VERIFY_REQUEST_ALREADY_MADE.value:
            return 'قبلا درخواست Verify داده شده است.'
        if code == BpCodes.VERIFY_REQUEST_NOT_FOUND.value:
            return 'درخواست Verify یافت نشد.'
        if code == BpCodes.SETTLED_TRANSACTION.value:
            return 'تراکنش Settle شده است.'
        if code == BpCodes.UNSETTELED_TRANSACTION.value:
            return 'تراکنش Settle نشده است.'
        if code == BpCodes.SETTLE_TRANSACTION_NOT_FOUND.value:
            return 'تراکنش Settle یافت نشد.'
        if code == BpCodes.RESERVED_TRANSACTION.value:
            return 'تراکنش Reserve یافت نشد.'
        if code == BpCodes.REFUND_TRANSACTION_NOT_FOUND.value:
            return 'تراکنش Refund یافت نشد.'
        if code == BpCodes.INVALID_BILL_CODE.value:
            return 'شناسه قبض نادرست است.'
        if code == BpCodes.INVALID_PAYMENT_CODE.value:
            return 'شناسه پرداخت نادرست است.'
        if code == BpCodes.INVALID_BILL_ISSUER.value:
            return 'سازمان صادرکننده قبض نامعتبر است.'
        if code == BpCodes.SESSION_TIMEOUT.value:
            return 'زمان جلسه کاری به پایان رسیده است.'
        if code == BpCodes.DATA_WRITE_ERROR.value:
            return 'خطا در ثبت اطلاعات.'
        if code == BpCodes.INVALID_PAYER_CODE.value:
            return 'شناسه پرداخت کننده نامعتبر است.'
        if code == BpCodes.COSTUMER_INFO_ERROR.value:
            return 'اشکال در تعریف اطلاعات مشتری'
        if code == BpCodes.EXCESSIVE_DATA_ENTRY.value:
            return 'تعداد دفعات ورود اطلاعات از حد مجاز گذشته است.'
        if code == BpCodes.INVALID_IP.value:
            return 'مقدار IP نامعتبر است.'
        if code == BpCodes.DUPLICATE_TRANSACTION.value:
            return 'تراکنش تکراری است.'
        if code == BpCodes.REFERENCE_TRANSACTION_NOT_FOUND.value:
            return 'تراکنش مرجع موجود نیست.'
        if code == BpCodes.INVALID_TRANSACTION.value:
            return 'تراکنش نامعتبر است.'
        if code == BpCodes.DEPOSIT_ERROR.value:
            return 'خطا در واریز.'





