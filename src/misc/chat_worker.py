import time
from threading import Thread
import queue

class ChatWorker:
    def __init__(self, app_instance, interval = 1, thread_count = 10):
        self.app = app_instance
        self.interval = interval
        self.thread_num = thread_count
        self.q = queue.Queue()
        self.threads = []
        self.active_requests = set()

        for i in range(thread_count):
            self.threads.append(Thread(target=self.add_chats, args=()))
            self.threads[i].daemon = True

    def start_thread(self):
        for i in range(self.thread_num):
            self.threads[i].start()

    def add_chats(self):
        while True:
            with self.app.app_context():
                from src.request.models import Request
                chat = self.q.get()
                try:
                    request = Request.objects.get(id=chat['request'])
                    request.add_chat_history(chat['chat'])
                except Exception as error:
                    print(error)
                time.sleep(self.interval)

    def enqueu_chat(self, chat):
        self.q.put(chat)
