from flask import flash, redirect, url_for, abort
from flask_login import current_user
from functools import wraps
from src.misc.constants import ROLES
from src.request.models import Request
from src.payment.models import Payment
from werkzeug.local import LocalProxy
from flask_login import current_user


# TODO remove request beacause of circular error

def ad_owner_or_admin(func):
    @wraps(func)
    def decorated_view(ad_id, *args, **kwargs):

        if any([ad.id == ad_id for ad in current_user.advertisements]) or current_user.role == ROLES['ADMIN']:
            return func(ad_id, *args, **kwargs)
        else:
            flash("you can't access to this page.")
            return redirect(url_for('user.advertisement', username=current_user.username))

    return decorated_view

def not_ad_owner(func):
    @wraps(func)
    def decorated_view(ad_id, *args, **kwargs):
        if any([ad.id == ad_id for ad in current_user.advertisements]) :
            flash("you can't access to this page.")
            return redirect(url_for('advertisement.ads_list', username=current_user.username))
        else:
            print('works')
            return func(ad_id, *args, **kwargs)


    return decorated_view


# TODO add logger
def req_owner_or_admin(func):
    @wraps(func)
    def decorated_view(req_id, *args, **kwargs):
        try:
            is_owner = Request.is_owner(current_user, req_id)
            if is_owner or current_user.role == ROLES['ADMIN']:
                return func(req_id, *args, **kwargs)
            else:
                flash("you can't access to this page.")
                return redirect(url_for('user.advertisement', username=current_user.username))
        except Exception as e:
            print(e)
            return abort(404)

    return decorated_view


def pay_owner_or_admin(func):
    @wraps(func)
    def decorated_view(pay_id, *args, **kwargs):

        try:
            is_owner = Payment.is_owner(current_user, pay_id)
            if is_owner or current_user.role == ROLES['ADMIN']:
                return func(pay_id, *args, **kwargs)
            else:
                flash("you can't access to this page.")
                return redirect(url_for('user.advertisement', username=current_user.username))
        except Exception as e:
            print(e)
            return abort(404)

    return decorated_view


def is_user_involved_dec(user):
    def real_decorator(func):
        @wraps(func)
        def checking(request_id, user=user):
            req = Request.get(request_id)
            try:
                if req.is_user_involved(username=user.username) or user.is_admin():
                    return func(user, request_id)
                else:
                    flash("you can't access to this page.")
                    return redirect(url_for('user.advertisement', username=user.username))
            except Exception as e:
                print(e)
                return abort(404)

        return checking

    return real_decorator


def just_admin(user):
    def real_decorator(func):
        @wraps(func)
        def checking(request_id, user=user):
            try:
                if user.role == 'ADMIN':
                    return func(user, request_id)
                else:
                    flash("you can't access to this page.")
                    return redirect(url_for('user.advertisement', username=user.username))
            except Exception as e:
                print(e)
                return abort(404)

        return checking

    return real_decorator


def test_dec(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        r = func(*args, **kwargs)
        return r

    return wrapped
