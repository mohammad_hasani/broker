from time import sleep
from threading import Thread
import queue
from .constants import TRANSACTION_STATUS, REQUEST_STATUS, PAYMENT_STATUS, TRANSACTION_HASH_STATUS, MODEL_NAMES
from .helpers import check_transaction_time, compare_times
from .bp_codes import BpCodes
from .exceptions import BpException, EthExplorerException
from .ethexplorer_codes import EthExplorerCodes
from src.cryptoapi.bitcoin_api import BitcoinApi
from src.cryptoapi.ethereum_api import EthApi
from src.request.models import Request
from src.payment.models import Payment
import pytz
from datetime import datetime
from moneywagon.core import NoService
from src.log.models import ExceptionLog, DatabaseObject
from src.misc.exception_log import ExceptionLogTypes
from src.user.models import User
import traceback

SATOSHI = 100000000


class PaymentWorker:
    def __init__(self, app_instance, interval=30, thread_count=1):
        self.app = app_instance
        self.interval = interval
        self.thread_num = thread_count
        self.q = queue.Queue()
        self.threads = []

        for i in range(thread_count):
            self.threads.append(Thread(target=self.check_buyer_pending_transactions, args=()))
            self.threads.append(Thread(target=self.check_seller_pending_transactions, args=()))
            self.threads.append(Thread(target=self.check_transaction_hashes, args=()))
            self.threads.append(Thread(target=self.check_request_confirmation, args=()))
            self.threads[i].daemon = True
            for i in range(thread_count * 4):
                self.threads[i].start()

    def check_buyer_pending_transactions(self):
        while True:
            with self.app.app_context():
                # Get all requests with transaction status == PENDING_BUYER_PAY
                try:
                    reqs = Request.get_buyer_pending_transactions()
                except Exception as e:
                    traceback.print_exc()
                    continue

                # Loop through requests and check if transactions are payed or timed out
                for req in reqs:
                    tx = req.transaction
                    if tx.payments:
                        payment = tx.payments[-1]
                        gateway_data = payment.gateway_data
                        # TODO: Make sure payment is not made. query the gateway.
                        if gateway_data.verify_res_code == BpCodes.SUCCESSFUL.value or \
                                gateway_data.inquiry_res_code == BpCodes.SUCCESSFUL.value:
                            tx.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                            # TODO: Notify seller and show etc
                            if payment.status != PAYMENT_STATUS['PAID']:
                                payment.status = PAYMENT_STATUS['PAID']
                                try:
                                    payment.save()
                                except Exception as e:
                                    traceback.print_exc()
                                    # TODO send email notification to admin
                            try:
                                req.save()
                                continue
                            except Exception as e:
                                traceback.print_exc()
                                # TODO send email notification to admin

                        if gateway_data.pay_res_code == BpCodes.SUCCESSFUL.value:
                            # TODO check that sale ref id and sale order id does exist, otherwise send to admin.

                            if gateway_data.verify_res_code is not None:
                                try:
                                    code = Payment.inquiry_payment(payment.order_id, gateway_data.sale_order_id,
                                                                   gateway_data.sale_reference_id)
                                    gateway_data.inquiry_res_code = code
                                    payment.status = PAYMENT_STATUS['PAID']
                                    payment.save()
                                    tx.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                                    req.save()
                                    continue
                                except BpException as e:
                                    gateway_data.inquiry_res_code = e
                                    payment.status = PAYMENT_STATUS['INVALID']
                                    payment.save()
                                except Exception as e:
                                    traceback.print_exc()
                            else:
                                try:
                                    code = Payment.verify_payment(payment.order_id, gateway_data.sale_order_id,
                                                                  gateway_data.sale_reference_id)
                                    gateway_data.verify_res_code = code
                                    payment.status = PAYMENT_STATUS['PAID']
                                    payment.save()
                                    tx.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                                    req.save()
                                    continue
                                except BpException as e:
                                    gateway_data.verify_res_code = e
                                    try:
                                        code = Payment.inquiry_payment(payment.order_id, gateway_data.sale_order_id,
                                                                       gateway_data.sale_reference_id)
                                        gateway_data.inquiry_res_code = code
                                        payment.status = PAYMENT_STATUS['PAID']
                                        payment.save()
                                        tx.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                                        req.save()
                                        continue
                                    except BpException as e:
                                        gateway_data.inquiry_res_code = e
                                        payment.status = PAYMENT_STATUS['INVALID']
                                        payment.save()
                                    except Exception as e:
                                        traceback.print_exc()
                                except Exception as e:
                                    traceback.print_exc()

                    if not check_transaction_time(tx.date, 1800):
                        # Fire an socket event to make transaction invalid in ui
                        try:
                            tx.status = TRANSACTION_STATUS['INVALID']
                            if tx.payments and tx.payments[-1].status != PAYMENT_STATUS['INVALID']:
                                payment.status = PAYMENT_STATUS['INVALID']
                                payment.save()
                            req.status = REQUEST_STATUS['BUYER_DIDNT_PAY']
                            req.save()
                            User.release_amount(req.ad_id, req.amount)
                        except Exception as error:
                            print('Error in payment worker: ' + str(error))
                            # Send email notification to admin
                    sleep(0.1)
                sleep(self.interval)

    def check_transaction_hashes(self):
        # TODO: Add two set queues. 1- Running Q. 2- Ready Q. Chcek if transaction is not queued in either of queues, if not add to ready queue.
        while True:
            try:
                # Get all seller pending transactions
                reqs = Request.get_not_checked_seller_pending_transactions()
            except Exception as e:
                traceback.print_exc()
                sleep(1)
                continue
                # Loop through seller pending requests.
            for req in reqs:
                # Get request's transaction object.
                tx = req.transaction
                # If seller submitted coin transaction hashes [Can be more than one.].
                if tx.seller_transaction_hashes:

                    # Loop through seller transaction hashes, and check one by one.
                    for tx_hash in tx.seller_transaction_hashes:
                        print(tx_hash.tx_hash)
                        # Check if transaction is checked before.
                        if TRANSACTION_HASH_STATUS['NOT_CHECKED'] not in tx_hash.status:
                            print('already checked')
                            continue

                        # Extract some variables for easier use.
                        buyer_wallet = req.buyer_wallet
                        seller_wallet = req.seller_wallet

                        # Handle with Bitcoin API.
                        if req.coin == 'BTC':
                            # Add UTC timezone to request time for comparing with seller transaction time.
                            req_time = pytz.utc.localize(req.date)
                            # Get transaction info from Bitcoin API.
                            try:
                                tx_info = BitcoinApi.get_transaction_info(tx_hash.tx_hash)
                            except NoService:
                                tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_CHECKED'])
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['INVALID'])
                                req.save()
                                continue
                            except Exception as e:
                                traceback.print_exc()
                                continue
                            print(tx_info)
                            # Extract some variables for easier use
                            outputs = tx_info['outputs']
                            inputs = tx_info['inputs']
                            time = tx_info['time']
                            confirmations = tx_info['confirmations']

                            # If transaction hash status is "Not enough confirmations" check for
                            #  new confirmations amount and if it is enough change status to ok.
                            if TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'] in tx_hash.status:
                                if confirmations > 2:
                                    tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'])
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['OK'])

                                    # Double check that valid transaction hashes only have one element.
                                    if len(tx_hash.status) > 1:
                                        print('Some error happened in tx_hash of request with id: {}'.format(req.id))
                                        # TODO: Notify admin.

                                    # Save request to database.
                                    try:
                                        req.save()
                                    except Exception as e:
                                        traceback.print_exc()
                                # Continue to next request.
                                continue

                            # Save seller transaction date and time.
                            tx_hash.date = time

                            # Check if request time is after seller transaction, and if it is
                            # after request time add "Differ By Date" status to seller transaction hash object.
                            if req_time > time:
                                print('differ by date')
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_DATE'])

                            # Extract Buyer Waller Outputs with seller wallet address from transaction.
                            buyer_wallet_outputs = [output for output in outputs if output['address'] == buyer_wallet]

                            # Check if buyer wallet outputs are more than one,
                            # notify admin to check manually.
                            if len(buyer_wallet_outputs) > 1:
                                print('two outputs with the same wallet in transaction.')
                                pass  # TODO: Notify admin that there are two outputs with the same buyer wallet in tx

                            # If no output with seller address found, add "Differ by buyer wallet" status
                            # to seller transaction hash object.
                            if not buyer_wallet_outputs:
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_BUYER_WALLET'])

                            # Extract seller wallet inputs with seller wallet address from transaction.
                            seller_wallet_inputs = [_input for _input in inputs if _input['address'] == seller_wallet]

                            # If there are no seller wallet inputs in transaction, add
                            # "Differ by seller wallet" status to seller transaction hash object.
                            if not seller_wallet_inputs:
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_SELLER_WALLET'])

                            # Get first output of buyer wallet outputs.
                            output = buyer_wallet_outputs[0]

                            # Convert output balance from satoshi in BTC.
                            amount_btc = output['amount'] / SATOSHI

                            # Save seller transaction hash amount.
                            tx_hash.amount = amount_btc

                            # Remove "Not checked" status from transaction hash.
                            try:
                                print(tx_hash.status)
                                print('removing not checked.')
                                tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_CHECKED'])
                                print(tx_hash.status)
                            except ValueError:
                                print('Transaction is checked before, but is being checked again. why?')

                            # If transaction hash status list is empty (meaning it didn't have any problems.).
                            if not tx_hash.status:
                                # If it hash enough confirmations
                                if confirmations >= 3:
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['OK'])
                                else:
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'])
                            try:
                                req.save()
                            except Exception as e:
                                traceback.print_exc()


                        elif req.coin == 'ETH':
                            req_time = req.date
                            try:
                                tx_info = EthApi.get_transaction_info(tx_hash.tx_hash)
                            except EthExplorerException as e:
                                if int(e) in (EthExplorerCodes.INVALID_ADDRESS_FORMAT.value,
                                              EthExplorerCodes.INVALID_TRANSACTION_HASH_FORMAT,
                                              EthExplorerCodes.TRANSACTION_NOT_FOUND):
                                    tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_CHECKED'])
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['INVALID'])
                            except Exception as e:
                                traceback.print_exc()
                                continue
                            from_address = tx_info['from']
                            to_address = tx_info['to']
                            timestamp = tx_info['timestamp']
                            amount_eth = tx_info['value']
                            time = datetime.utcfromtimestamp(timestamp)
                            success = tx_info['success']
                            confirmations = tx_info['confirmations']

                            tx_hash.date = time
                            tx_hash.amount = amount_eth
                            print(seller_wallet)
                            print(from_address)

                            # If transaction hash status is "Not enough confirmations" check for
                            #  new confirmations amount and if it is enough change status to ok.
                            if TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'] in tx_hash.status:
                                if confirmations >= 12:
                                    tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'])
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['OK'])

                                    # Double check that valid transaction hashes only have one element.
                                    if len(tx_hash.status) > 1:
                                        print('Some error happened in tx_hash of request with id: {}'.format(req.id))
                                        # TODO: Notify admin.

                                    # Save request to database.
                                    try:
                                        req.save()
                                    except Exception as e:
                                        traceback.print_exc()
                                # Continue to next request.
                                continue
                            print(time)
                            print(req_time)
                            if req_time > time:
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_DATE'])

                            if to_address != buyer_wallet:
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_BUYER_WALLET'])

                            if from_address != seller_wallet:
                                tx_hash.status.append(TRANSACTION_HASH_STATUS['DIFFER_BY_SELLER_WALLET'])

                            try:
                                print(tx_hash.status)
                                print('removing not checked.')
                                tx_hash.status.remove(TRANSACTION_HASH_STATUS['NOT_CHECKED'])
                                print(tx_hash.status)
                            except ValueError:
                                print('Transaction is checked before, but is being checked again. why?')

                            if not tx_hash.status:
                                # If it hash enough confirmations
                                if confirmations >= 3:
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['OK'])
                                else:
                                    tx_hash.status.append(TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS'])

                            try:
                                req.save()
                            except Exception as e:
                                traceback.print_exc()
                        else:
                            print('Req %s coin is not supported.' % req.id)
                sleep(10)

    def check_seller_pending_transactions(self):
        while True:
            # Get all request with "Pending seller pay" status that has transaction hash.
            try:
                reqs = Request.get_all_seller_pending_transaction_hashes()
            except Exception as e:
                traceback.print_exc()
                sleep(1)
                continue

            # Loop through requests.
            for req in reqs:
                # Extract request time in utc timezone in a variable.
                req_time = req.date
                if [hash for hash in req.transaction.seller_transaction_hashes if
                    (TRANSACTION_HASH_STATUS['NOT_CHECKED'] or TRANSACTION_HASH_STATUS['INVALID']) not in hash.status]:
                    continue

                if not check_transaction_time(req_time, 3600):
                    # If there wasn't any tx hash.
                    if (not req.transaction.seller_transaction_hashes or not [hash for hash in
                                                                              req.transaction.seller_transaction_hashes
                                                                              if
                                                                              TRANSACTION_HASH_STATUS['OK'] or
                                                                              TRANSACTION_HASH_STATUS[
                                                                                  'NOT_ENOUGH_CONFIRMATIONS'] in hash.status]):
                        req.update(status=REQUEST_STATUS['SELLER_DIDNT_PAY'])
                        User.release_amount(req.ad_id, req.amount)
                        if req.transaction.payments[-1].status != PAYMENT_STATUS['PAID']:
                            ExceptionLog.objects.create(type=ExceptionLogTypes.INVALID_STATUS.value, file=__file__,
                                                        database_objects=[
                                                            DatabaseObject(model_name=MODEL_NAMES['REQUEST'],
                                                                           id=req.id),
                                                            DatabaseObject(model_name=MODEL_NAMES['PAYMENT'],
                                                                           id=req.transaction.payments[-1].id)])
                        req.transaction.payments[-1].update(needs_refund=True)
                        continue
                sum = 0
                for tx_hash in req.transaction.seller_transaction_hashes:
                    if len(tx_hash.status) == 1 and tx_hash.status[0] == TRANSACTION_HASH_STATUS['OK']:
                        sum += tx_hash.amount
                last_tx_hash = req.transaction.seller_transaction_hashes[-1]
                try:
                    if sum >= req.amount:
                        # Check whether seller paid in time.
                        if compare_times(req_time, last_tx_hash.date, 3600):
                            # Update transaction status.
                            req.update(transaction__status=TRANSACTION_STATUS['SELLER_PAID'])
                            User.release_amount(req.ad_id, req.amount)
                        else:
                            req.update(transaction__status=TRANSACTION_STATUS['SELLER_PAID_AFTER_TIMEOUT'])
                            User.release_amount(req.ad_id, req.amount)
                        continue
                except Exception as e:
                    traceback.print_exc()
                    continue

                sum = 0
                for tx_hash in req.transaction.seller_transaction_hashes:
                    if len(tx_hash.status) == 1 and (
                            tx_hash.status[0] == TRANSACTION_HASH_STATUS['OK'] or tx_hash.status[0] ==
                            TRANSACTION_HASH_STATUS['NOT_ENOUGH_CONFIRMATIONS']):
                        sum += tx_hash.amount
                    print(tx_hash.tx_hash)
                last_tx_hash = req.transaction.seller_transaction_hashes[-1]
                print(last_tx_hash.tx_hash)
                try:
                    if not compare_times(req_time, last_tx_hash.date, 86400):
                        if sum >= req.amount:
                            # Update transaction status.
                            req.update(status=REQUEST_STATUS['CONFIRMATION_TAKING_TOO_LONG'])
                            User.release_amount(req.ad_id, req.amount)
                        elif sum > 0:
                            # Update transaction status.
                            req.update(status=REQUEST_STATUS['SELLER_DIDNT_PAY_ENOUGH'])
                            User.release_amount(req.ad_id, req.amount)
                            if req.transaction.payments[-1].status != PAYMENT_STATUS['PAID']:
                                ExceptionLog.objects.create(type=ExceptionLogTypes.INVALID_STATUS.value, file=__file__,
                                                            database_objects=[
                                                                DatabaseObject(model_name=MODEL_NAMES['REQUEST'],
                                                                               id=req.id),
                                                                DatabaseObject(model_name=MODEL_NAMES['PAYMENT'],
                                                                               id=req.transaction.payments[-1].id)])
                            req.transaction.payments[-1].update(needs_refund=True)
                        elif sum <= 0:
                            # Update transaction status.
                            req.update(status=REQUEST_STATUS['SELLER_DIDNT_PAY'])
                            User.release_amount(req.ad_id, req.amount)
                            if req.transaction.payments[-1].status != PAYMENT_STATUS['PAID']:
                                ExceptionLog.objects.create(type=ExceptionLogTypes.INVALID_STATUS.value, file=__file__,
                                                            database_objects=[
                                                                DatabaseObject(model_name=MODEL_NAMES['REQUEST'],
                                                                               id=req.id),
                                                                DatabaseObject(model_name=MODEL_NAMES['PAYMENT'],
                                                                               id=req.transaction.payments[-1].id)])
                            req.transaction.payments[-1].update(needs_refund=True)
                        continue
                except Exception as e:
                    traceback.print_exc()
                    continue
            sleep(10)

    def check_request_confirmation(self):
        while True:
            try:
                reqs = Request.get_seller_paid_transactions()
            except Exception as e:
                traceback.print_exc()
                sleep(1)
                continue

            for req in reqs:
                if not check_transaction_time(req.date, 86400):
                    try:
                        req.update(status=REQUEST_STATUS['DONE'])
                    except Exception as e:
                        traceback.print_exc()
                        continue
