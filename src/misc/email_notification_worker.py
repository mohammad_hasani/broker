# import time
# from threading import Thread
# import queue
from flask_mail import Message
from src.misc.helpers import get_email_template
from src import celery, app, mail
#
# class EmailWorker:
#     def __init__(self, email_server_instance, app_instance, interval=5, thread_num=2):
#         self.app = app_instance
#         self.email_server = email_server_instance
#         self.interval = interval
#         self.thread_num = thread_num
#         self.q = queue.Queue()
#         self.threads = []
#
#         for i in range(thread_num):
#             self.threads.append(Thread(target=self.send_email, args=()))
#             self.threads[i].daemon = True
#             self.threads[i].start()
#
#     def send_email(self):
#         while True:
#             with self.app.app_context():
#                 message = self.q.get()
#                 print(message)
#                 self.email_server.send(message)
#                 print('Email Sent.')
#                 time.sleep(self.interval)
#
#     def enqueue_email(self, receiver, subject, body, template_name=None, **kwargs):
#         if template_name:
#             template = get_email_template(template_name)
#             message = Message(subject=subject, body=body, html=template, recipients=[receiver])
#             self.q.put(message)
#         elif 'html' in kwargs:
#             message = Message(subject=subject, body=body, html=kwargs['html'], recipients=[receiver])
#             self.q.put(message)
#         else:
#             raise ValueError('Template name or HTML must be set correctly.')
#
#     def enqueue_email_object(self, email_object):
#         if isinstance(email_object, Message):
#             self.q.put(email_object)
#         else:
#             raise ValueError('Email object must be instance of Flask-Mail Message class.')


def create_email(receiver, subject, body, template_name=None, **kwargs):
    if template_name:
        template = get_email_template(template_name)
        message = Message(subject=subject, body=body, html=template, recipients=[receiver])
        return message
    elif 'html' in kwargs:
        message = Message(subject=subject, body=body, html=kwargs['html'], recipients=[receiver])
        return message
    else:
        raise ValueError('Template name or HTML must be set correctly.')

@celery.task
def send_async_email(receiver, subject, body, template_name=None, **kwargs):
    print('got an email to send.')
    with app.app_context():
        try:
            msg = create_email(receiver, subject, body, template_name, **kwargs)
        except ValueError:
            raise
        mail.send(msg)

