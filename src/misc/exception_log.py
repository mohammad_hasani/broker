from enum import Enum

class ExceptionLogTypes(Enum):
    CANNOT_VERIFY_OR_INQUIRY = 0
    NOT_UNIQUE_OUTPUT_ADDRESS = 1
    INVALID_STATUS = 2