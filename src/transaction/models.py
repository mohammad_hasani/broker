from src import db
from src.misc.constants import CONFLICT_STATUS, TRANSACTION_STATUS


class Payment(db.EmbeddedDocument):
    pay_id = db.StringField()
    status = db.StringField()
    price_in_rials = db.DecimalField(min_value=0, precision=8)


class Transaction(db.Document):
    request = db.StringField()
    item = db.StringField()
    amount = db.DecimalField(min_value=0, precision=8)
    price = db.DecimalField(min_value=0, precision=8)
    total_price = db.DecimalField(min_value=0, precision=8)
    feedback_score = db.IntField(min_value=1, max_value=5)
    date = db.DateTimeField()
    seller_shaba = db.StringField()
    seller_account_number = db.StringField()
    status = db.StringField(choices=TRANSACTION_STATUS)
    feedback = db.BooleanField()
    conflict_status = db.StringField(choices=CONFLICT_STATUS)
    conflict_date = db.DateTimeField()
    payment_history = db.ListField(db.EmbeddedDocumentField(Payment))

    def create(self):
        return self.save()

    def update_status(self, status):
        self.status = TRANSACTION_STATUS[status]
        return self.save()

    def update(self, **kwargs):
        for key, value in kwargs.items():
            self[key] = value
        return self.save()

    @classmethod
    def get(cls, id):
        return cls.objects.get(pk=id)

    def get_status(self):
        return self.status
