from flask import Blueprint

transaction_blueprint = Blueprint('transaction', __name__, template_folder='templates')

from . import views