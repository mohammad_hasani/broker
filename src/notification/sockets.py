from flask import url_for
from flask_socketio import emit, join_room, leave_room, rooms
from flask_login import login_required, current_user
from src import socketio


@login_required
def notification(usernames, endpoint, args, type):
    for username in usernames:
        url = url_for(endpoint, **args)

        socketio.emit('notification', data={'url': url,'type':type}, room=username)
