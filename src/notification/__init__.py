from flask.blueprints import Blueprint

notification_blueprint = Blueprint('notification', __name__, template_folder='templates')
from .import views
from .import sockets
