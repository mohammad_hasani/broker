from src import db
from src.misc.constants import NOTIFICATION_TYPE, SEEN_STATUS
from .sockets import notification as notification_socket
import json


class NotificationDetail(db.EmbeddedDocument):
    id = db.StringField()
    type = db.StringField(choices=NOTIFICATION_TYPE)
    url_endpoint = db.StringField()
    args = db.StringField(default='')
    kwargs = db.StringField()
    text = db.StringField()
    seen = db.StringField(default=SEEN_STATUS['UNSEEN'], choices=SEEN_STATUS)


class SeenNotificationDetail(db.EmbeddedDocument):
    id = db.StringField()
    type = db.StringField(choices=NOTIFICATION_TYPE)
    url_endpoint = db.StringField()
    args = db.StringField(default='')
    kwargs = db.StringField()
    text = db.StringField()
    seen = db.StringField(default=SEEN_STATUS['SEEN'], choices=SEEN_STATUS)


class Notification(db.Document):
    username = db.StringField(unique=True, required=True)
    detail = db.ListField(db.EmbeddedDocumentField(NotificationDetail))
    seen = db.ListField(db.EmbeddedDocumentField(SeenNotificationDetail))

    @classmethod
    def append_notification(cls, username, notification_detail):
        '''notification is an object of Notification class'''
        if notification_detail == None:
            return cls.objects().create(username=username)
        else:
            return cls.objects(username=username).update_one(push__detail=notification_detail, upsert=True)

    @staticmethod
    def send_notification(username, notification_detail=None):
        # save notification to db
        result = Notification.append_notification(username, notification_detail)

        if result and notification_detail != None:
            # send notification through socket
            notification_socket([username], notification_detail['url_endpoint'],
                                json.loads(notification_detail['args']),
                                notification_detail['type'])

        return result

    @classmethod
    def notification_count(cls, username):
        all_notif = cls.objects.get(username=username)
        all_notif_count = int(len(all_notif['detail']))

        seen_notif = 0
        for i in all_notif['detail']:
            if i['seen'] == SEEN_STATUS['SEEN']:
                seen_notif += 1

        unseen_notif = all_notif_count - seen_notif

        return {'all': all_notif_count, 'seen': seen_notif, 'unseen': unseen_notif}

    @classmethod
    def get_unseen_notification(cls, username):
        # TODO CHANGE DATABASE TO SEEN AND UNSEEN INSTEAD OF DETAIL AND GET ALL INSTEAD OF 40
        all_notif = cls.objects.get(username=username)
        all_notif_count = int(len(all_notif['detail']))

        unseen_notif = []
        for i in all_notif.detail[:40]:
            if i.seen == SEEN_STATUS['UNSEEN']:
                unseen_notif.append(i)

        for i in unseen_notif:
            i.args = json.loads(i.args)
        return {'unseen_notif': unseen_notif}
