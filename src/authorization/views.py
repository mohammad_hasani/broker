from flask import render_template, request, redirect, url_for, flash, session, abort, g
from flask_login import current_user
from mongoengine import errors as MongoEngineExceptions
from flask_login import login_required

from src.authorization import authorization_blueprint

from .forms import AuthorizationForm
from .models import Authorization
from src.user.models import User

@authorization_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def authorize():
    session.setdefault('count', 1)
    form = AuthorizationForm()
    total = session.get('count')
    for i in range(total):
        if not form.is_submitted():
            form.credit_cards.append_entry()

        if form.validate_on_submit():
            try:
                if Authorization.save_authorization_info(current_user.username, form.fname.data,
                                                      form.lname.data, form.melli_code.data,
                                                      form.authorization_photo.data, form.credit_cards.data):
                    flash('successfully saved.')
                    return redirect(url_for('authorization.authorize'))
                else:
                    flash('cant save duplicate authorization info, '
                          'please contact our support if you have any problem with your authorization info.')
                    return redirect(url_for('authorization.authorize'))
            except MongoEngineExceptions.DoesNotExist:
                flash('user does not exist.')
                return redirect(url_for('authorization.authorize'))

    return render_template('authorization.html',user=current_user, form=form)


@authorization_blueprint.route('/add/', methods=['GET', 'POST'])
@login_required
def add_form_item():
    session['count'] = int(session['count']) + 1
    return ''