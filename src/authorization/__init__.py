from flask import Blueprint

authorization_blueprint = Blueprint(
    'authorization', __name__, template_folder='templates')

from . import views
