from flask_wtf import FlaskForm
from wtforms import StringField, FieldList, FormField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES
from wtforms.validators import InputRequired, Length

# images = UploadSet('images', IMAGES, app.config['UPLOAD_FOLDER'])

class AuthorizationForm(FlaskForm):
    fname = StringField('First Name', id='fname', validators=[
                        InputRequired(), Length(min=2, max=128)])
    lname = StringField('Last Name', id='lname', validators=[
                        InputRequired(), Length(min=2, max=256)])
    melli_code = StringField('Melli Code', id='melli_code', validators=[
                             InputRequired(), Length(min=10, max=10)])
    credit_cards = FieldList(StringField('Credit Card'))
    authorization_photo = FileField('Authorization photo containing melli card, id card, and authorization code.',
                                    id='authorization_photo', validators=[FileRequired()])
    # captcha field

