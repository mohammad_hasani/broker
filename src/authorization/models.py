from src.user.models import User
from datetime import datetime
import os
from src import app
from src.misc.constants import AUTHORIZATION_STATUS, USER_INFO_STATUS

class Authorization():
    @staticmethod
    def save_authorization_info(username, fname, lname, melli_code, authorization_photo, credit_cards):
        user = User.get(username)
        if user.authorization_status != AUTHORIZATION_STATUS['AUTHORIZED']:
            if user.authorization_photo_status != USER_INFO_STATUS['ACCEPTED']:
                filename = username + '_'+ str(datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')) + os.path.splitext(authorization_photo.filename)[1]
                authorization_photo.save(os.path.join(
                    app.config['UPLOAD_FOLDER'], 'authorization_photos', filename
                ))
                user.authorization_photo = filename
                user.authorization_photo_status = USER_INFO_STATUS['PENDING']

            for credit_card in credit_cards:
                if credit_card not in user.credit_cards:
                    user.credit_cards.append(credit_card)
            if user.credit_cards_status in (USER_INFO_STATUS['NOT_FILLED'], USER_INFO_STATUS['REJECTED']):
                user.credit_cards_status = USER_INFO_STATUS['PENDING']
            if user.fname != USER_INFO_STATUS['ACCEPTED']:
                user.fname = fname
                user.fname_status = USER_INFO_STATUS['PENDING']
            if user.lname_status != USER_INFO_STATUS['ACCEPTED']:
                user.lname = lname
                user.lname_status = USER_INFO_STATUS['PENDING']
            if user.melli_code_status != USER_INFO_STATUS['ACCEPTED']:
                user.melli_code = melli_code
                user.melli_code_status = USER_INFO_STATUS['PENDING']
            user.authorization_status = AUTHORIZATION_STATUS['PENDING']
            user.save()
            return True
        else:
            return False