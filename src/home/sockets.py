from flask_login import login_required, current_user
from src import socketio
from flask_socketio import send, emit, join_room, leave_room
from flask import request

@socketio.on('join_personal_room')
@login_required
def join_personal_room():
    room = current_user.username
    print(current_user.username)
    join_room(room)


# @socketio.on('disconnect')
# @login_required
# def disconnect(x):
#     leave_room(room=current_user.username)
