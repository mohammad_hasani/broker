from flask import render_template, request, redirect, url_for, flash, session, abort
from flask_login import login_required, login_user, logout_user, current_user
from src.home import home_blueprint
from src.user.models import User
from json import dumps


@home_blueprint.route('/')
@login_required
def index():
    return render_template('home.html', title='Home')


@home_blueprint.route('/ad-list/')
def ad_list(ad_id=None):
    ad_list = User.get_ads_paginated(request.args)
    return render_template('ad-list.html', ad_list=ad_list)
