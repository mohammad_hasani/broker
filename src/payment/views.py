from flask import render_template, redirect, url_for, flash, abort, request
from flask_login import login_required, current_user
from . import payment_blueprint
from src.payment.models import Payment
from src.request.models import Request
from mongoengine import errors as MongoEngineExceptions
from src.misc.constants import TRANSACTION_STATUS
from src.misc.bp_codes import BpCodes
from src.misc.exceptions import BpException
from src.misc.constants import BANKTEST_GATEWAY, PAYMENT_DIRECTION, PAYMENT_TYPE, PAYMENT_STATUS
from src.misc.helpers import check_transaction_time


@payment_blueprint.route('/pay/<string:request_id>/')
@login_required
def coin_payment(request_id):
    print('coin payment route.')
    # TODO check transaction doesnt have pending payments or is not payed before. if it has NOT_PAYED payment check it with beh pardakht and mark that invalid first.
    # TODO also check if transaction has other payments that are not statused invalid. if it has check those with beh pardakht and if one of them is already payed do not let user pay, and set transaction status to pending seller pay.
    # TODO: Ta 15 daqiqe betone dobare ye tx ro pay kone, ama badesh dg madresh gaeede mishe.
    try:
        # Get Request
        req = Request.get(request_id)

        tx = req.transaction
        if tx.status == TRANSACTION_STATUS['INVALID'] or not check_transaction_time(tx.date, 900):
            abort(400, description='Transaction status is invalid or you were already paying.')

        if not req.mark_payments_invalid():
            flash('You already have payed, you don\'t need to pay again.')
            return redirect(url_for('request.detail', request_id=request_id))

        # Generate an order id for gateway
        order_id = Payment.get_unique_id()
        total_price = int(tx.total_price)
        payment = Payment(order_id=order_id, direction=PAYMENT_DIRECTION['IN'],
                          type=PAYMENT_TYPE['COIN_PAYMENT'], user=req.buyer,
                          total_price=total_price, commission_price=req.commission_price,
                          request=req)

        # Save payment
        payment.save()
        ref_id = None
        # Get Ref Id from gateway
        ref_id = Payment.get_payment_ref_id(payment.id, total_price, order_id)
        if not ref_id:
            flash('Gateway error.')
            return redirect(url_for('request.detail', request_id=request_id))
        print('ref id: ' + ref_id)
        # Create a payment object to be added to transaction
        payment.update(gateway_data__ref_id=ref_id)
        print('payment updated with ref id')
        tx.payments.append(payment)
        # Save changes
        req.save()
        print('request saved.')
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except BpException as e:
        print('Bp Exception:')
        print(e)
        if e == BpCodes.CARD_EXPIRED.value:
            flash('Card is expired.')
            return redirect(url_for('request.detail', request_id=request_id))
    except Exception as e:
        print(e)
        abort(500)
    return render_template('payment.html', ref_id=ref_id, gateway=BANKTEST_GATEWAY)


# check payment and update tx status.
@payment_blueprint.route('/verify-payment/', methods=['GET', 'POST'])
@login_required
def verify_payment():
    if request.method == 'POST':
        try:
            ref_id = request.form['RefId']
            res_code = request.form['ResCode']
            sale_order_id = request.form['SaleOrderId']
            sale_reference_id = request.form['SaleReferenceId']
            card_holder_pan = request.form.get('CardHolderPan')
            card_holder_info = request.form.get('CardHolderInfo')

            payment = Payment.get_by_order_id(sale_order_id)
            # Sanity Check
            if ref_id != payment.gateway_data.ref_id:
                flash('Payment ref id is not valid.')
            payment.gateway_data.pay_res_code = res_code
            payment.gateway_data.sale_order_id = sale_order_id
            payment.gateway_data.sale_reference_id = sale_reference_id
            payment.gateway_data.card_holder_pan = card_holder_pan
            payment.gateway_data.card_holder_info = card_holder_info
            if int(res_code) == BpCodes.SUCCESSFUL.value:
                print('successful.')
                flash(BpCodes.get_message_for_res_code(res_code))
                is_verified = False
                try:
                    code = Payment.verify_payment(sale_order_id, sale_order_id, sale_reference_id)
                    payment.gateway_data.verify_res_code = code
                    payment.status = PAYMENT_STATUS['PAID']
                    payment.request.transaction.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                    is_verified = True
                except BpException as e:
                    payment.gateway_data.verify_res_code = e
                    try:
                        code = Payment.inquiry_payment(sale_order_id, sale_order_id, sale_reference_id)
                        payment.gateway_data.inquiry_res_code = code
                        payment.status = PAYMENT_STATUS['PAID']
                        payment.request.transaction.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
                        is_verified = True
                    except BpException as e:
                        payment.gateway_data.inquiry_res_code = e
                        # payment.status = PAYMENT_STATUS['INVALID']
                        # TODO: Should I make payment invalid or not. Cause it'll be checked again in payment worker.
                if is_verified:
                    try:
                        code = Payment.settle_payment(sale_order_id, sale_order_id, sale_reference_id)
                        payment.gateway_data.settle_res_code = code
                    except BpException as e:
                        payment.gateway_data.settle_res_code = e
                        print('payment could not be settled.')
            else:
                print('unsuccessful.')
                flash(BpCodes.get_message_for_res_code(res_code))
                payment.status = PAYMENT_STATUS['INVALID']
            try:
                payment.save()
                payment.request.save()
            except Exception as e:
                # TODO: Do something like sending data for admin if payment could be saved.
                print(e)
                abort(500)
        except KeyError:
            flash('Payment data is incomplete.')
            # TODO: Do something like sending data for admin if payment could be saved.
        return redirect(url_for('request.detail', request_id=payment.request.id))

@payment_blueprint.route('/detail/<string:payment_id>/')
@login_required
def detail(payment_id):
    try:
        payment = Payment.get(payment_id)
        return payment.status
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        return e

@login_required
@payment_blueprint.route('/withdrawal-payments/')
def withdrawal_payments():
    try:
        payments = Payment.get_all_withdrawals(current_user.username)
    except Exception as e:
        print(e)
        abort(500)
    return render_template('withdrawal-payments.html', payments=payments)