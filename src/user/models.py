from src import db
from flask_login import UserMixin
from mongoengine.queryset.visitor import Q
from src.misc.exceptions import InvalidClientData, NotFound, IncorrectPassword
from uuid import uuid4
from src.misc.circular import coin_list
from src.misc.constants import AUTHORIZATION_STATUS, ROLES, \
    USER_STATUS, USER_INFO_STATUS, EMAIL_NOTIFICATION, \
    PRICE_MODELS, COMMISSION_TYPE, ADVERTISEMENT_STATUS, \
    REPLY_TIME, WALLET_TYPE, REQUEST_STATUS, ENABLED_SITE_SETTING
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
import os
from config import UPLOAD_FOLDER
from src.notification.models import Notification
from src.sitesetting.models import SiteSetting
import json


class Wallet(db.EmbeddedDocument):
    name = db.StringField()
    coin = db.StringField()
    address = db.StringField()
    id = db.StringField()
    # TODO age wallet ad rush dasht wallet natune pak she. va natune ruye in wallet add bezare


class BankAccount(db.EmbeddedDocument):
    account_number = db.StringField()
    shaba_code = db.StringField()
    is_default = db.BooleanField()


class AuthorizationError(db.EmbeddedDocument):
    for_field = db.StringField()
    desc = db.StringField()


class Advertisement(db.EmbeddedDocument):
    id = db.StringField()
    price_model = db.StringField(choices=PRICE_MODELS)
    dollar_price = db.DecimalField(min_value=0, precision=8)
    per_unit_price = db.DecimalField(min_value=0, precision=8)
    min_amount = db.DecimalField(min_value=0, precision=8)
    max_amount = db.DecimalField(min_value=0, precision=8)
    coin = db.StringField()
    date = db.DateTimeField()
    wallet_type = db.StringField(choices=WALLET_TYPE)
    wallet = db.StringField()
    commission_type = db.StringField(choices=COMMISSION_TYPE)
    status = db.StringField(choices=ADVERTISEMENT_STATUS)
    reserved_amount = db.DecimalField(default=0, min_value=0, precision=8)
    private = db.BooleanField()

    requests = db.ListField(db.ReferenceField('Request'))

    def __repr__(self):
        return '[Ad] Coin: %s, Min Amount: %s, Max Amount: %s, Id: %s' % (
            self.coin, str(self.min_amount), str(self.max_amount), self.id)


# TODO change "commision" to "commission" , ss !, change it everywhere
class User(UserMixin, db.Document):
    phonenumber = db.StringField(max_length=13, required=True, unique=True)
    phonenumber_status = db.StringField(default=USER_INFO_STATUS['PENDING'])
    username = db.StringField(max_length=50, required=True, unique=True)
    email = db.EmailField(max_length=128)
    email_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    password = db.StringField(min_length=8, max_length=128, required=True)
    fname = db.StringField(max_length=128)
    fname_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    lname = db.StringField(max_length=256)
    lname_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    credit_cards = db.ListField(db.StringField())
    credit_cards_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    melli_code = db.StringField(min_length=10, max_length=10)
    melli_code_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    authorization_photo = db.StringField()
    authorization_photo_status = db.StringField(default=USER_INFO_STATUS['NOT_FILLED'])
    authorization_id = db.StringField()
    wallets = db.ListField(db.EmbeddedDocumentField(Wallet))
    authorization_status = db.StringField(default=AUTHORIZATION_STATUS['NOT_AUTHORIZED'], choices=AUTHORIZATION_STATUS)
    role = db.StringField(default=ROLES['USER'], choices=ROLES)
    status = db.StringField(default=USER_STATUS['ACTIVE'], choices=USER_STATUS)
    authorization_errors = db.ListField(db.EmbeddedDocumentField(AuthorizationError))
    commission_discount = db.FloatField(min_value=0, max_value=1, default=0)
    sale_count = db.IntField(default=0, min_value=0)
    feedback_count = db.IntField(default=0, min_value=0)
    feedback_sum = db.FloatField(default=0, min_value=0)
    email_notifications = db.ListField(db.StringField(choices=EMAIL_NOTIFICATION))
    email_verification_code = db.StringField()
    profile_photo = db.StringField()
    reset_password_status = db.StringField(default=USER_INFO_STATUS['NORMAL'])
    reply_time_average = db.StringField(choices=REPLY_TIME, default='NOT_CALCULATED')
    reply_sum = db.DecimalField(default=0, min_value=0)
    request_answered_count = db.IntField(default=0, min_value=0)
    advertisements = db.ListField(db.EmbeddedDocumentField('Advertisement'))
    bank_accounts = db.ListField(db.EmbeddedDocumentField(BankAccount))
    archived_wallets = db.ListField(db.EmbeddedDocumentField(Wallet))
    registration_time = db.DateTimeField()
    notifications = db.ReferenceField('Notification', required=True)
    meta = {
        'indexes': ['phonenumber', 'username']
    }

    def __repr__(self):
        return 'User %s' % self.phonenumber

    @classmethod
    def get(cls, phonenumber_or_email_or_username):
        user = cls.objects.get(
            Q(phonenumber=phonenumber_or_email_or_username) | Q(email=phonenumber_or_email_or_username) | Q(
                username=phonenumber_or_email_or_username))

        for notification_detail in user.notifications.detail:
            notification_detail.args = json.loads(notification_detail.args)
        return user

    def update(self, **kwargs):
        for key, value in kwargs.items():
            self[key] = value
        return self.save()

    @classmethod
    def getWithUsername(cls, username):
        return cls.objects.get(username=username)

    def add_wallet(self, name, coin, address):
        if not coin in [coin['symbol'] for coin in coin_list]:
            raise InvalidClientData
        id = uuid4().hex
        wallet = Wallet(name=name, coin=coin, address=address, id=id)
        self.wallets.append(wallet)
        return self.save()

    def edit_wallet(self, id, name, coin, address):
        wallet = [w for w in self.wallets if w.id == id]
        print(wallet[0])
        if wallet:
            wallet[0]['name'] = name
            wallet[0]['coin'] = coin
            wallet[0]['address'] = address
            return self.save()
        raise NotFound

    def remove_wallet(self, id):
        wallet = [w for w in self.wallets if w.id == id]
        if wallet:
            self.wallets.remove(wallet[0])
            return self.save()
        raise NotFound

    def check_unsent_feedback(self):
        pass

    def add_email_notification(self, name):
        self.email_notifications.append(name)
        return self.save()

    def has_email_notification_for(self, name):
        if name in self.email_notifications:
            return True
        return False

    def verify_email(self):
        self.email_status = USER_INFO_STATUS['ACCEPTED']
        return self.save()

    def change_password(self, current_password, new_password):
        if check_password_hash(self.password, current_password):
            self.password = generate_password_hash(new_password)
            return self.save()
        raise IncorrectPassword

    def reset_password(self, new_password):
        self.password = generate_password_hash(new_password)
        return self.save()

    def change_profile_photo(self, username, profile_photo):
        filename = username + '_' + str(datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')) + \
                   os.path.splitext(profile_photo.filename)[1]
        profile_photo.save(os.path.join(
            UPLOAD_FOLDER, 'profile_photos', filename
        ))
        self.profile_photo = filename
        return self.save()

    @classmethod
    def get_ad_for_all(cls, ad_id):
        user = cls.get_user_by_ad_id(ad_id)
        return next(ad for ad in user.advertisements if ad.id == ad_id), user

    def get_ad_for_self(self, ad_id):
        return next(ad for ad in self.advertisements if ad.id == ad_id)

    @classmethod
    def get_user_by_ad_id(cls, ad_id):
        return cls.objects.get(advertisements__id=ad_id)

    # TODO: Go to request
    # @staticmethod
    # def is_requested_before(item_id, username):
    #     from src.request.models import Request
    #     try:
    #         Request.objects.get(Q(status='PENDING') | Q(status='ACCEPTED'), item=item_id,
    #                             buyer=username)
    #         return True
    #     except:
    #         return False

    # TODO: check wallet balance before reserving
    @classmethod
    def reserve_amount(cls, ad_id, amount):
        ad, user = cls.get_ad_for_all(ad_id)
        if not ad.reserved_amount:
            ad.reserved_amount = 0
        ad.reserved_amount += amount
        return user.save()

    @classmethod
    def release_amount(cls, ad_id, amount):
        ad, user = cls.get_ad_for_all(ad_id)
        ad.reserved_amount -= amount
        if ad.reserved_amount < 0:
            return user.save()
        else:
            print(
                'Cant release amount. Amount after releasing must be greater or equal to zero. Resetting reserved amount to zero')
            ad.reserved_amount = 0
            return user.save()

    @classmethod
    def append_ad(cls, username, ad):
        return cls.objects(username=username).update_one(push__advertisements=ad)

    def ad_is_requested_before(self, ad_id, username):
        ad = self.get_ad_for_self(ad_id)
        return any([req.buyer == username and req.status in [REQUEST_STATUS['PENDING']] for req in ad.requests])

    def append_request(self, ad_id, request):
        ad = self.get_ad_for_self(ad_id)
        ad.requests.append(request)
        return self.save()

    def change_commission_discount(self, commision_discount):
        if type(commision_discount) == float:
            self.commision_discount = commision_discount
            return self.save()
        raise IndentationError

    def is_admin(self):
        return self.role == ROLES['ADMIN']

    @classmethod
    def ad_archive(cls, ad_id):
        return cls.objects(advertisements__id=ad_id).update(
            set__advertisements__S__status=ADVERTISEMENT_STATUS['DEACTIVE'])

    @classmethod
    def get_ads_paginated(cls, filters):
        query = cls.query_builder(filters)
        res = cls._get_collection().aggregate(query)
        res = list(res)
        return res

    @classmethod
    def increment_feedback(cls, username, score):
        res = cls.objects(username=username).modify(inc__feedback_sum=score, inc__feedback_count=1)
        return res

    @staticmethod
    def query_builder(filters):
        page = int(filters.get('page')) if filters.get('page') else 1
        limit = int(filters.get('limit')) if filters.get('limit') else 10
        offset = (page - 1) * limit
        query = [
            {"$unwind": "$advertisements"},
            {
                "$match": {"advertisements.status": "ACTIVE", "advertisements.private": False}
            },
            {"$sort": {"advertisements.date": -1}},
            {"$limit": limit},
            {"$skip": offset},
            {"$project": {
                "ad": "$advertisements",
                "username": "$username",
                "reply": "$reply_time_average",
                "feedback_count": "$feedback_count",
                "feedback_sum": "$feedback_sum"}
            }
        ]
        coin = filters.get('coin')
        if coin:
            query[1]['$match']['advertisements.coin'] = coin

        min_amount_gt = filters.get('min_amount_gt')
        min_amount_lt = filters.get('min_amount_lt')
        if min_amount_gt and min_amount_lt:
            min_amount_gt = float(min_amount_gt)
            min_amount_lt = float(min_amount_lt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$gte": min_amount_gt,
                                                                                 "$lte": min_amount_lt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$gte": min_amount_gt,
                                                                                 "$lte": min_amount_lt}})
        elif min_amount_gt:
            min_amount_gt = float(min_amount_gt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$gte": min_amount_gt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$gte": min_amount_gt}})
        elif min_amount_lt:
            min_amount_lt = float(min_amount_lt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$lte": min_amount_lt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.min_amount": {"$lte": min_amount_lt}})

        max_amount_gt = filters.get('max_amount_gt')
        max_amount_lt = filters.get('max_amount_lt')
        if max_amount_gt and max_amount_lt:
            max_amount_gt = float(max_amount_gt)
            max_amount_lt = float(max_amount_lt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$gte": max_amount_gt,
                                                                                 "$lte": max_amount_lt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$gte": max_amount_gt,
                                                                                 "$lte": max_amount_lt}})
        elif max_amount_gt:
            max_amount_gt = float(max_amount_gt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$gte": max_amount_gt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$gte": max_amount_gt}})
        elif max_amount_lt:
            max_amount_lt = float(max_amount_lt)
            if query[1]['$match'].get('$and'):
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$lte": max_amount_lt}})
            else:
                query[1]['$match']['$and'] = []
                query[1]['$match']['$and'].append({"advertisements.max_amount": {"$lte": max_amount_lt}})

        commission_type = filters.get('commission_type')
        if commission_type:
            query[1]['$match']['advertisements.commission_type'] = commission_type
            # feedback = filters.get('feedback')
        return query

    # REPLY_TIME METHODS
    def reply_average(self):
        result = self.reply_sum / self.request_answered_count
        if result <= 5:
            self.reply_time_average = REPLY_TIME['UNDER_5_MINS']
        elif 5 < result <= 30:
            self.reply_time_average = REPLY_TIME['UNDER_30_MINS']
        elif 30 < result:
            self.reply_time_average = REPLY_TIME['MORE_THAN_30_MINS']
        return

        # @classmethod
        # def add_notification():
        #     pass
