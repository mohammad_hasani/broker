from flask import render_template, flash, redirect, url_for, abort, request,jsonify
from flask_login import login_required, current_user, logout_user
from .forms import AddWalletForm, EmailForm, ChangePasswordForm, ResetPasswordForm, ProfilePhotoForm,ChangeCommissionDiscount
from . import user_blueprint
from .models import User
from mongoengine import errors as MongoEngineExceptions
from src.misc.exceptions import InvalidClientData, NotFound, IncorrectPassword, EthExplorerException
from src.misc.constants import EMAIL_NOTIFICATION, USER_INFO_STATUS
from src.misc.helpers import get_verification_link
from uuid import uuid4
from src.misc.requires_roles import requires_roles
from src.cryptoapi.ethereum_api import EthApi
from src.cryptoapi.bitcoin_api import BitcoinApi
from src.misc.exceptions import EthExplorerException



#TODO add commision discount
#TODO admin can change commsion discount for specefic user
@user_blueprint.route('/<string:username>/')
@login_required
def profile(username):
    # try:
    #     data = EthApi.check_tx_received('0x95eff588db625357a3795ffaed9dc5d7bc827c9d', '0x7270b4e9de09d5dc21222d0c9937a49089a96efa', 0.031, '2017-12-22 22:33:58')
    #     print(data)
    # except EthExplorerException as e:
    #     print(e)
    user = None
    if username == current_user.username:
        return render_template('profile.html', user=current_user, username=username)
    try:
        user = User.getWithUsername(username)
    except MongoEngineExceptions.DoesNotExist:
        flash('User not found.')
        abort(404)
    except MongoEngineExceptions.NotUniqueError:
        flash('Multiple users returned.')
        abort(500)
    except:
        flash('Database error.')
        abort(500)
    return render_template('profile.html', user=user, username=username)

@user_blueprint.route('/wallets/add/', methods=['GET', 'POST'])
@login_required
def add_wallet():
    form = AddWalletForm()
    if form.validate_on_submit():
        try:
            if form.coin.data=='BTC':
                valid_wallet = BitcoinApi.get_wallet_balance(form.address.data)
            elif form.coin.data=='ETH':
                valid_wallet = EthApi.get_wallet_info(form.address.data)
        except EthExplorerException as e:
            flash('your wallet is invalid')
            return redirect(url_for("user.add_wallet"))
        except Exception as e:
            flash("it seems there is a problem. try again .")
            return redirect(url_for("user.add_wallet"))

        try:
            current_user.add_wallet(form.name.data, form.coin.data, form.address.data)
            flash('Successfully added.')
            return redirect(url_for('user.profile', username=current_user.username))
        except MongoEngineExceptions.DoesNotExist:
            flash('User not found.')
            return redirect(url_for('user.add_wallet'))
        except MongoEngineExceptions.NotUniqueError:
            flash('Multiple users returned.')
            return redirect(url_for('user.add_wallet'))
        except InvalidClientData:
            flash('Selected coin is disabled, you cannot add wallet address for disabled coins.')
            return redirect(url_for('user.add_wallet'))
        except Exception as e:
            print(e)
            flash('Database error.')
            return redirect(url_for('user.add_wallet'))
    return render_template('add-wallet.html', form=form)


@user_blueprint.route('/wallets/remove/<string:id>/', methods=['GET', 'POST'])
@login_required
def remove_wallet(id):
        try:
            current_user.remove_wallet(id)
            flash('Successfully removed wallet.')
            return redirect(url_for('user.profile', username=current_user.username))
        except MongoEngineExceptions.DoesNotExist:
            flash('User not found.')
            return redirect(url_for('user.profile', username=current_user.username))
        except MongoEngineExceptions.NotUniqueError:
            flash('Multiple users returned.')
            return redirect(url_for('user.profile', username=current_user.username))
        except NotFound:
            flash('Wallet not found.')
            return redirect(url_for('user.profile', username=current_user.username))
        except Exception as e:
            print(e)
            flash('Database error.')
            return redirect(url_for('user.profile', username=current_user.username))


@user_blueprint.route('/wallets/edit/<string:id>/', methods=['GET', 'POST'])
@login_required
def edit_wallet(id):
    form = AddWalletForm()
    if request.method == 'GET':
        try:
            current_data = [w for w in current_user.wallets if w.id == id][0]
        except IndexError:
            abort(404)
        form.coin.data = current_data.coin
        form.address.data = current_data.address
        form.name.data = current_data.name
    if form.validate_on_submit():
        try:
            current_user.edit_wallet(id, form.name.data, form.coin.data, form.address.data)
            flash('Successfully edited.')
            return redirect(url_for('user.profile', username=current_user.username))
        except MongoEngineExceptions.DoesNotExist:
            flash('User not found.')
            return redirect(url_for('user.profile', username=current_user.username))
        except MongoEngineExceptions.NotUniqueError:
            flash('Multiple users returned.')
            return redirect(url_for('user.profile', username=current_user.username))
        except NotFound:
            flash('Wallet does not exist.')
            return redirect(url_for('user.profile', username=current_user.username))
        except Exception as e:
            print(e)
            flash('Database error.')
            return redirect(url_for('user.profile', username=current_user.username))
    return render_template('edit-wallet.html', form=form)

@user_blueprint.route('/notification/email/add/<string:name>/')
@login_required
def add_email_notification(name):
    if name not in current_user.email_notifications and name in EMAIL_NOTIFICATION.keys() and current_user.email != None:
        try:
            current_user.add_email_notification(EMAIL_NOTIFICATION[name])
            flash('successfully saved.')
        except Exception as error:
            print(error)
            flash('Database error.')
            return redirect(url_for('user.profile', username=current_user.username))
        return redirect(url_for('user.profile', username=current_user.username))
    else:
        flash('already have or not found.')
        return redirect(url_for('user.profile', username=current_user.username))

@user_blueprint.route('/change-email/<string:username>/', methods=['GET', 'POST'])
@login_required
def change_email(username):
    form = EmailForm()
    if form.validate_on_submit():
        try:
            user = User.get(username)
            user.update(email=form.email.data, email_status=USER_INFO_STATUS['PENDING'],
                        email_verification_code=uuid4().hex)
            link = get_verification_link(user.email_verification_code, username)
            # from src import email_worker
            # email_worker.enqueue_email(form.email.data, 'Verification for Securypto', link, html=link)
            flash(
                'email change successfully to %s. a verification email is sent to your new email, please check it.' % form.email.data)
            return redirect(url_for('user.profile', username=username))
        except MongoEngineExceptions.DoesNotExist:
            abort(404)
        except Exception as error:
            print(error)
            return abort(500)
    try:
        user = User.get(username)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as error:
        print(error)
        return abort(500)
    return render_template('change_email.html', form=form, user=user)


@user_blueprint.route('/change-password/', methods=['GET', 'POST'])
@login_required
def change_password():
    reset_code = request.args.get('reset_code')
    is_reset = True if current_user.email_verification_code == reset_code else False
    if reset_code != None and not is_reset:
        flash('reset code is wrong. probably used a password reset link twice.')
        return redirect(url_for('authentication.login'))
    if is_reset:
        form = ResetPasswordForm()
    else:
        form = ChangePasswordForm()
    if form.validate_on_submit():
        try:
            if is_reset:
                current_user.reset_password(form.password.data)
                current_user.update(reset_password_status=USER_INFO_STATUS['NORMAL'], email_verification_code=uuid4().hex)
            else:
                current_user.change_password(form.current_password.data, form.password.data)
            flash('password changed successfully.')
            logout_user()
            return redirect(url_for('authentication.login'))
        except IncorrectPassword:
            flash('current password is not correct.')
        except Exception as error:
            print(error)
            abort(500)
    return render_template('change-password.html', form=form, reset_code=reset_code)

@user_blueprint.route('/change-profile-photo/', methods=['GET', 'POST'])
@login_required
def change_profile_photo():
    form = ProfilePhotoForm()
    if form.validate_on_submit():
        try:
            current_user.change_profile_photo(current_user.username, form.profile_photo.data)
            flash('changed successfully.')
            return redirect(url_for('user.profile', username=current_user.username))
        except Exception as error:
            print(error)
            abort(500)
    return render_template('change-profile-photo.html', form=form)


@login_required
@requires_roles('ADMIN')
@user_blueprint.route('/change_commission_discount/<string:username>/',methods=['get','post'])
def change_commission_discount(username):
    form = ChangeCommissionDiscount()

    if form.validate_on_submit():
        User.objects(username=username).update_one(commision_discount=form.commission.data)
        flash('commission discount change for user '+ username, category='commission')
        return redirect(url_for('user.change_commission_discount', username=username))
    else:
        user = User.getWithUsername(username)
        return render_template('change_commission_discount.html', form=form, username=username,user=user)