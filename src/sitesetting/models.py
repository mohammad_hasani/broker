from src import db
from src.misc import constants
from mongoengine.errors import DoesNotExist

class Offer(db.EmbeddedDocument):
    end_date = db.DateTimeField()
    discount = db.IntField()


class SiteSetting(db.Document):
    commission_rate = db.FloatField(min_value=0, max_value=100, default=1)
    admin_items_per_page = db.IntField(default=10)
    roles = db.ListField(db.StringField())
    ticket_types = db.ListField(db.StringField())
    name = db.StringField(required=True, unique=True)
    meta = {'max_documents': 5, 'indexes': ['name']}
    offer = db.EmbeddedDocumentField(Offer)

    def initialize(self, name='DEFAULT'):
        try:
            SiteSetting.objects.get(name=name)
        except DoesNotExist:
            default_setting = SiteSetting(name=name)
            default_setting.roles.append('ADMIN')
            default_setting.roles.append('USER')
            default_setting.ticket_types.append('FINANCIAL')
            default_setting.ticket_types.append('TECHNICAL')
            default_setting.save()
            return self.initialize()

        default_setting = SiteSetting.objects.get(name=name)
        if not 'ADMIN' in default_setting.roles:
            default_setting.roles.append('ADMIN')
        if not 'USER' in default_setting.roles:
            default_setting.roles.append('USER')
        if not 'FINANCIAL' in default_setting.ticket_types:
            default_setting.ticket_types.append('FINANCIAL')
        if not 'TECHNICAL' in default_setting.ticket_types:
            default_setting.ticket_types.append('TECHNICAL')

        constants.ADMIN_ITEMS_PER_PAGE = default_setting.admin_items_per_page
        constants.ROLES = {v: v for v in default_setting.roles}
        constants.TICKET_TYPES = {v: v for v in default_setting.ticket_types}

    def change_setting(self, name, **kwargs):
        SiteSetting.objects(name=name).update_one(**kwargs)
        self.set_setting(name)

    def set_setting(self, name):
        self.initialize(name)


