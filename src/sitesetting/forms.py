from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, SelectField, PasswordField, FloatField
from wtforms.validators import Length, InputRequired, Email, EqualTo
from src.coin.models import Coin


class SiteSettingForm(FlaskForm):
    commision_rate = FloatField(label='global commission rate')