from src.user.models import User, Wallet, BankAccount, Advertisement
from src.misc.constants import USER_INFO_STATUS, AUTHORIZATION_STATUS, \
    ROLES, USER_STATUS, EMAIL_NOTIFICATION, COMMISSION_TYPE, PAYMENT_DIRECTION, \
    PAYMENT_STATUS, PAYMENT_TYPE, REQUEST_STATUS, TRANSACTION_STATUS
from werkzeug.security import generate_password_hash
from mongoengine.errors import DoesNotExist
from config import UPLOAD_FOLDER
import numpy
from PIL import Image
from uuid import uuid4
import os
import random
from faker import Faker
from src.misc.helpers import get_rand_int
from datetime import datetime
from src.request.models import Request
from src.payment.models import Payment, GatewayData
from src.notification.models import Notification


def seed_my_user():
    try:
        User.get(phonenumber_or_email_or_username='09363525151')
        message = 'User already exists.'
        print(message)
        return message
    except DoesNotExist:
        pass
    filename = 'pooyakn_' + uuid4().hex
    path = os.path.join(UPLOAD_FOLDER, 'authorization_photos', filename)
    path += '.png'
    print(path)
    imarray = numpy.random.rand(100, 100, 3) * 255
    im = Image.fromarray(imarray.astype('uint8')).convert('RGBA')
    im.save(path)
    fbc = random.randint(0, 100)
    score = round(random.uniform(1, 5))
    notifications = Notification.send_notification(username='pooyakn')
    sample_user = User(phonenumber='09363525151',
                       notifications=notifications,
                       phonenumber_status=USER_INFO_STATUS['ACCEPTED'],
                       username='pooyakn',
                       email='pooya.kn.73@gmail.com',
                       email_status=USER_INFO_STATUS['ACCEPTED'],
                       password=generate_password_hash('6211724'),
                       fname='Pooya',
                       fname_status=USER_INFO_STATUS['ACCEPTED'],
                       lname='Kermani',
                       lname_status=USER_INFO_STATUS['ACCEPTED'],
                       credit_cards=['6280231440838661'],
                       credit_cards_status=USER_INFO_STATUS['ACCEPTED'],
                       melli_code='0797849031',
                       melli_code_status=USER_INFO_STATUS['ACCEPTED'],
                       authorization_photo=filename,
                       authorization_photo_status=USER_INFO_STATUS['ACCEPTED'],
                       authorization_id=''.join(random.choice('0123456789ABCDEF') for i in range(6)),
                       wallets=[Wallet(name='Pooya Wallet', coin='BTC', address='1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX',
                                       id=uuid4().hex)],
                       authorization_status=AUTHORIZATION_STATUS['AUTHORIZED'],
                       role=ROLES['ADMIN'],
                       status=USER_STATUS['ACTIVE'],
                       commission_discount=0.5,
                       feedback_sum=fbc * score,
                       feedback_count=fbc,
                       email_notifications=[EMAIL_NOTIFICATION['NEW_REQUEST']],
                       email_verification_code=uuid4().hex,
                       profile_photo=filename,
                       bank_accounts=[BankAccount(account_number='1234567890', shaba_code='IR89394839848983593895',
                                                  is_default=True)])
    try:
        sample_user.save()
        message = 'Database seeded successfully.'
        return message
    except Exception as e:
        message = 'Cant save user due to error:' + '\n' + e
        print(message)
        return message


def seed_fake_user(password, username=None):
    fake = Faker()
    username = username if username else fake.user_name()
    filename = '' + uuid4().hex
    path = os.path.join(UPLOAD_FOLDER, 'authorization_photos', filename)
    path += '.png'
    print(path)
    imarray = numpy.random.rand(100, 100, 3) * 255
    im = Image.fromarray(imarray.astype('uint8')).convert('RGBA')
    im.save(path)
    fbc = random.randint(0, 100)
    score = round(random.uniform(1, 5))
    notifications = Notification.send_notification(username=username)
    fake_user = User(phonenumber=get_rand_int(10),
                     notifications=notifications,
                     phonenumber_status=USER_INFO_STATUS['ACCEPTED'],
                     username=username,
                     email=fake.email(),
                     email_status=USER_INFO_STATUS['ACCEPTED'],
                     password=generate_password_hash(password),
                     fname=fake.first_name(),
                     fname_status=USER_INFO_STATUS['ACCEPTED'],
                     lname=fake.last_name(),
                     lname_status=USER_INFO_STATUS['ACCEPTED'],
                     credit_cards=[fake.credit_card_number() for x in range(0, random.randrange(1, 5))],
                     credit_cards_status=USER_INFO_STATUS['ACCEPTED'],
                     melli_code=get_rand_int(10),
                     melli_code_status=USER_INFO_STATUS['ACCEPTED'],
                     authorization_photo=filename,
                     authorization_photo_status=USER_INFO_STATUS['ACCEPTED'],
                     authorization_id=''.join(random.choice('0123456789ABCDEF') for i in range(6)),
                     wallets=[
                         Wallet(name=username + ' wallet', coin='BTC', address='1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX',
                                id=uuid4().hex)],
                     authorization_status=AUTHORIZATION_STATUS['AUTHORIZED'],
                     role=ROLES['USER'],
                     status=USER_STATUS['ACTIVE'],
                     commission_discount=1,
                     feedback_sum=fbc * score,
                     feedback_count=fbc,
                     email_notifications=[EMAIL_NOTIFICATION['NEW_REQUEST']],
                     email_verification_code=uuid4().hex,
                     profile_photo=filename,
                     bank_accounts=[
                         BankAccount(account_number=get_rand_int(10), shaba_code=get_rand_int(10), is_default=True)])
    try:
        fake_user.save()
        message = 'Database seeded successfully.'
        print(message)
        return fake_user.to_json()
    except Exception as e:
        message = 'Cant save user due to error:' + '\n' + str(e)
        print(message)
        return message


def seed_admin_user(password, username=None):
    fake = Faker()
    username = username if username else fake.user_name()
    filename = '' + uuid4().hex
    path = os.path.join(UPLOAD_FOLDER, 'authorization_photos', filename)
    path += '.png'
    print(path)
    imarray = numpy.random.rand(100, 100, 3) * 255
    im = Image.fromarray(imarray.astype('uint8')).convert('RGBA')
    im.save(path)
    fbc = random.randint(0, 100)
    score = round(random.uniform(1, 5))
    notifications = Notification.send_notification(username=username)
    fake_user = User(phonenumber=get_rand_int(10),
                     notifications=notifications,
                     phonenumber_status=USER_INFO_STATUS['ACCEPTED'],
                     username=username,
                     email=fake.email(),
                     email_status=USER_INFO_STATUS['ACCEPTED'],
                     password=generate_password_hash(password),
                     fname=fake.first_name(),
                     fname_status=USER_INFO_STATUS['ACCEPTED'],
                     lname=fake.last_name(),
                     lname_status=USER_INFO_STATUS['ACCEPTED'],
                     credit_cards=[fake.credit_card_number() for x in range(0, random.randrange(1, 5))],
                     credit_cards_status=USER_INFO_STATUS['ACCEPTED'],
                     melli_code=get_rand_int(10),
                     melli_code_status=USER_INFO_STATUS['ACCEPTED'],
                     authorization_photo=filename,
                     authorization_photo_status=USER_INFO_STATUS['ACCEPTED'],
                     authorization_id=''.join(random.choice('0123456789ABCDEF') for i in range(6)),
                     wallets=[
                         Wallet(name=username + ' wallet', coin='BTC', address='1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX',
                                id=uuid4().hex)],
                     authorization_status=AUTHORIZATION_STATUS['AUTHORIZED'],
                     role=ROLES['ADMIN'],
                     status=USER_STATUS['ACTIVE'],
                     commission_discount=1,
                     feedback_sum=fbc * score,
                     feedback_count=fbc,
                     email_notifications=[EMAIL_NOTIFICATION['NEW_REQUEST']],
                     email_verification_code=uuid4().hex,
                     profile_photo=filename,
                     bank_accounts=[
                         BankAccount(account_number=get_rand_int(10), shaba_code=get_rand_int(10), is_default=True)])
    try:
        fake_user.save()
        message = 'Database seeded successfully.'
        print(message)
        return fake_user.to_json()
    except Exception as e:
        message = 'Cant save user due to error:' + '\n' + str(e)
        print(message)
        return message


def create_advertisement(username):
    try:
        user = User.get(username)
        ad = Advertisement(id=uuid4().hex, min_amount=round(random.uniform(0.1, 1), 2),
                           max_amount=round(random.uniform(1, 10), 2),
                           coin=['BTC', 'ETH'][random.randrange(0, 2)], date=datetime.utcnow(),
                           price_model='PER_UNIT', per_unit_price=520000, private=False,
                           wallet=user.wallets[0]['id'],
                           commission_type=['BUYER_PAYS', 'SELLER_PAYS', 'FIFTY_FIFTY'][random.randrange(1, 3)],
                           status='ACTIVE')
        user.advertisements.append(ad)
        user.save()
        return 'Done'
    except Exception as e:
        print(e)
        return e


def create_payment(req_id, status):
    req = Request.get(req_id)
    if req.status != REQUEST_STATUS['ACCEPTED']:
        return 'First accept your request.'
    order_id = random.randint(10, 1000)
    total_price = req.transaction.total_price
    gateway_data = GatewayData(ref_id='ABCEDFGHIJK', pay_res_code=0,
                               sale_order_id=order_id,
                               sale_reference_id=random.randint(10000, 90000),
                               verify_res_code=0,
                               settle_res_code=0)

    payment = Payment(order_id=order_id, direction=PAYMENT_DIRECTION['IN'],
                      type=PAYMENT_TYPE['COIN_PAYMENT'], total_price=total_price,
                      status=PAYMENT_STATUS[status], commission_price=req.commission_price, gateway_data=gateway_data)

    payment.save()
    req.transaction.payments.append(payment)
    req.transaction.status = TRANSACTION_STATUS['PENDING_SELLER_PAY']
    req.save()
    return payment.to_json()
