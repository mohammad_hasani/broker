from flask import render_template
from . import devtools_blueprint
from .models import seed_my_user, seed_fake_user, seed_admin_user, create_advertisement, create_payment
from src.user.models import User
from src.request.models import Request



@devtools_blueprint.route('/seedmyuser/')
def _my_user():
    result = seed_my_user()
    return result


@devtools_blueprint.route('/seedfake/')
@devtools_blueprint.route('/seedfake/<string:username>/<string:password>/')
def seed_fake(username=None, password='6211724'):
    user = seed_fake_user(username=username, password=password)
    return user


@devtools_blueprint.route('/seedadmin/<string:username>/<string:password>/')
def seed_admin(username=None, password='123456789'):
    user = seed_admin_user(username=username, password=password)
    return user


@devtools_blueprint.route('/add-advertisement/<string:username>/')
def add_advertisement(username):
    ad = create_advertisement(username)
    return ad


@devtools_blueprint.route('/archive/')
def archive():
    User.ad_archive('278061eecfd44063b0b184a493a2f48f')
    # u=User()
    # print(u.get_ad('278061eecfd44063b0b184a493a2f48f'))
    return 'done'


@devtools_blueprint.route('/viewreqs/')
def view_request():
    reqs = Request.objects()
    return render_template('view-requests.html', requests=reqs)


@devtools_blueprint.route('/addpayment/<string:req_id>/')
@devtools_blueprint.route('/addpayment/<string:req_id>/<string:status>/')
def add_payment(req_id, status=None):
    if not status:
        status = 'PAID'
    result = create_payment(req_id, status)
    return result
