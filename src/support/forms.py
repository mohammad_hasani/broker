from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, TextAreaField
from flask_wtf.file import FileField, FileRequired
from wtforms.validators import InputRequired, Length
from src.misc.constants import TICKET_TYPES


class TicketForm(FlaskForm):
    ticket_type = SelectField('Ticket Type', id='ticket_type', choices=list(TICKET_TYPES.items()), validators=[InputRequired()])
    # related_product = SelectField(choices=??)
    title = StringField('Title', id='title', validators=[InputRequired(), Length(min=3, max=1024)])
    message_text = TextAreaField('Message', id='message_text', validators=[InputRequired(), Length(min=5, max=2048)])
    message_media = FileField('Attachment', id='message_media')

class AnswerForm(FlaskForm):
    message_text = TextAreaField('Message', id='message_text', validators=[InputRequired(), Length(min=5, max=2048)])
    message_media = FileField('Attachment', id='message_media')