from src import db
from src.misc.constants import TICKET_TYPES, CHAT_TYPES, TICKET_STATUS


class Message(db.EmbeddedDocument):
    sender = db.StringField()
    text = db.StringField()
    media = db.StringField()
    media_type = db.StringField(choices=CHAT_TYPES)
    date = db.DateTimeField()

    def __repr__(self):
        return 'Message \n Sender: {}, Text: {}'.format(self.sender, self.text)

class Ticket(db.Document):
    type = db.StringField(choices=TICKET_TYPES)
    date = db.DateTimeField()
    title = db.StringField()
    sender = db.StringField()
    messages = db.ListField(db.EmbeddedDocumentField(Message))
    associated_admin = db.StringField()
    related_product = db.StringField()
    status = db.StringField(default=TICKET_STATUS['PENDING_ADMIN_ANSWER'], choices=TICKET_STATUS)
    meta = {'indexes': ['sender']}


    @classmethod
    def get(cls, id):
        return cls.objects.get(pk=id)