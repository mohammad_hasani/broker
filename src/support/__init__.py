from flask import Blueprint

support_blueprint = Blueprint('support', __name__, template_folder='templates')

from . import views
from . import sockets