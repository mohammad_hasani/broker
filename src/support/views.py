from flask import render_template, flash, redirect, url_for, abort
from flask_login import login_required, current_user
from src.support import support_blueprint
from src.misc.requires_roles import requires_roles
from src.misc.exceptions import NotFound
from src.misc.constants import TICKET_STATUS
from .forms import TicketForm, AnswerForm
from .models import Ticket, Message
from datetime import datetime
import os
from src import app
from src.misc.helpers import get_type_from_extension, get_iran_datetime
from mongoengine import errors as MongoEngineExceptions
from uuid import uuid4


@support_blueprint.route('/ticket/add/', methods=['GET', 'POST'])
@login_required
def add_ticket():
    form = TicketForm()
    if form.validate_on_submit():
        filename = None
        if form.message_media.data is not None:
            ext = os.path.splitext(form.message_media.data.filename)[1]
            try:
                media_type = get_type_from_extension(ext)
            except TypeError:
                flash('File Extension %s is not allowed.' % ext)
                return redirect(url_for('support.add_ticket'))
            filename = current_user.username + '_' + str(datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')) + '_' + uuid4().hex + ext
            form.message_media.data.save(os.path.join(
                app.config['UPLOAD_FOLDER'], 'ticket_media', filename
            ))

            ticket = Ticket(type=form.ticket_type.data, date=datetime.utcnow(), title=form.title.data,
                            messages=[Message(sender=current_user.username, text=form.message_text.data,
                                              media=filename, media_type=media_type,
                                              date=datetime.utcnow())],
                                              sender=current_user.username, related_product='NONE')
        else:
            ticket = Ticket(type=form.ticket_type.data, date=datetime.utcnow(), title=form.title.data,
                            messages=[Message(sender=current_user.username, text=form.message_text.data,
                                              media=filename, date=datetime.utcnow())],
                                              sender=current_user.username, related_product='NONE')
        try:
            ticket.save()
            flash('Saved Successfully')
            return redirect(url_for('support.view_ticket', id=ticket.id))
        except MongoEngineExceptions.ValidationError:
            flash('You data is not valid.')
            return redirect(url_for('support.add_ticket'))

    return render_template('add_ticket.html', form=form)


@support_blueprint.route('/ticket/view/<string:id>/', methods=['GET', 'POST'])
@login_required
def view_ticket(id):
    form = AnswerForm()
    try:
        ticket = Ticket.get(id)
        if ticket.sender != current_user.username and current_user.role != 'ADMIN':
            raise NotFound
    except MongoEngineExceptions.DoesNotExist:
        return abort(404)
    except NotFound:
        return abort(404)
    except Exception as error:
        print(error)
        flash('Database error.')
        return abort(500)

    if form.validate_on_submit():
        filename = None
        message = None
        if form.message_media.data is not None:
            ext = os.path.splitext(form.message_media.data.filename)[1]
            try:
                media_type = get_type_from_extension(ext)
            except TypeError:
                flash('File Extension %s is not allowed.' % ext)
                return redirect(url_for('support.add_ticket'))
            filename = current_user.username + '_' + str(
                datetime.utcnow().strftime('%Y-%m-%d_%H-%M-%S')) + '_' + uuid4().hex + ext
            form.message_media.data.save(os.path.join(
                app.config['UPLOAD_FOLDER'], 'ticket_media', filename
            ))
            message = Message(sender=current_user.username, text=form.message_text.data,
                              media=filename, media_type=media_type, date=datetime.utcnow())
        else:
            message = Message(sender=current_user.username, text=form.message_text.data, date=datetime.utcnow())

        try:
            ticket.messages.append(message)
            if current_user.role == 'ADMIN' and current_user == ticket.sender:
                ticket.status = TICKET_STATUS['PENDING_ADMIN_ANSWER']
            else:
                ticket.status = TICKET_STATUS['PENDING_USER_ANSWER']
            ticket.save()
        except MongoEngineExceptions.ValidationError as error:
            print(error)
            flash('message cant be validated.')
            return redirect(url_for('support.view_ticket', id=ticket.id))
        except Exception as error:
            print(error)
            flash('Database error.')
            abort(500)

    return render_template('view_ticket.html', form=form, ticket=ticket, datetime=get_iran_datetime(ticket.date), id=id)


@support_blueprint.route('/ticket/list/')
@support_blueprint.route('/ticket/list/<string:username>/')
@login_required
def list_tickets(username = None):
    if username == None:
        tickets = Ticket.objects(sender=current_user.username)
        return render_template('list_tickets.html', tickets=tickets, get_iran_datetime=get_iran_datetime)

    if current_user.username == username or current_user.role == 'ADMIN':
        tickets = Ticket.objects(sender=username)
        return render_template('list_tickets.html', tickets=tickets, get_iran_datetime=get_iran_datetime)
    else:
        abort(404)


@support_blueprint.route('/ticket/close/<string:id>/')
@login_required
def close_ticket(id):
    try:
        ticket = Ticket.get(id)
        if ticket.sender != current_user.username and current_user.role != 'ADMIN':
            raise NotFound
        ticket.status = TICKET_STATUS['CLOSED']
        ticket.save()
        flash('ticket closed successfully.')
        return redirect(url_for('support.view_ticket', id=ticket.id))
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except NotFound:
        abort(404)
    except Exception as error:
        print(error)
        flash('Database error in closing ticket.')
        return redirect(url_for('support.view_ticket', id=id))


@support_blueprint.route('/ticket/list_unanswered/')
@login_required
@requires_roles('ADMIN')
def list_unanswered_tickets():
    try:
        tickets = Ticket.objects(status=TICKET_STATUS['PENDING_ADMIN_ANSWER'])
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as error:
        print(error)
        flash('Database error in getting unanswered tickets.')
        return redirect(url_for('admin.admin'))
    return render_template('list_unanswered_tickets.html', tickets=tickets, get_iran_datetime=get_iran_datetime)