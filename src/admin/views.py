from flask import render_template, flash, redirect, url_for, request, abort
from flask_login import login_required
from src.misc.requires_roles import requires_roles
from . import admin_blueprint
from src.admin.models import Admin
from src.user.models import User
from mongoengine import errors as MongoEngineExceptions
from .forms import AuthorizationErrorForm, EmailTemplateForm
from src.misc.constants import ROLES, PAYMENT_DIRECTION, PAYMENT_STATUS, PAYMENT_TYPE
from src.payment.models import Payment
from src.log.models import ExceptionLog

@admin_blueprint.route('/')
@login_required
@requires_roles('ADMIN')
def admin():
    return render_template('index.html')


@admin_blueprint.route('/ban/<string:username>/')
@login_required
@requires_roles('ADMIN')
def ban(username):
    try:
        Admin.ban_user(username)
        flash('User %s banned successfully.' % username)
    except MongoEngineExceptions.DoesNotExist:
        flash('User does not exist.')
    except MongoEngineExceptions.NotUniqueError:
        flash('Multiple users with %s exist.' % username)
    except Exception as error:
        print(error)
        flash('Database error.')
    return redirect(url_for('admin.get_users'))


@admin_blueprint.route('/unban/<string:username>/')
@login_required
@requires_roles('ADMIN')
def unban(username):
    try:
        Admin.unban_user(username)
        flash('User %s unbanned successfully.' % username)
    except MongoEngineExceptions.DoesNotExist:
        flash('User does not exist.')
    except MongoEngineExceptions.NotUniqueError:
        flash('Multiple users with %s exist.' % username)
    except Exception as error:
        print(error)
        flash('Database error.')
    return redirect(url_for('admin.get_users'))


@admin_blueprint.route('/elevate/<string:username>/')
@login_required
@requires_roles('ADMIN')
def elevate(username):
    try:
        Admin.elevate_user(username, ROLES['ADMIN'])
        flash('User %s elevated to %s successfully.' % (username, 'ADMIN'))
    except MongoEngineExceptions.DoesNotExist:
        flash('User does not exist.')
    except MongoEngineExceptions.NotUniqueError:
        flash('Multiple users with %s exist.' % username)
    except Exception as error:
        print(error)
        flash('Database error.')
    return redirect(url_for('admin.get_users'))


@admin_blueprint.route('/users/')
@admin_blueprint.route('/users/<int:page>/')
@login_required
@requires_roles('ADMIN')
def get_users(page=1):
    users = None
    try:
        users = Admin.get_users(page)
    except Exception as error:
        print(error)
        flash('Database error.')
    return render_template('view-users.html', users=users)


@admin_blueprint.route('/authorization-requests/')
@admin_blueprint.route('/authorization-requests/<int:page>/')
@login_required
@requires_roles('ADMIN')
def get_authorization_requests(page=1):
    try:
        requests = Admin.get_authorization_requests(page)
    except Exception as error:
        print(error)
        flash('Database error.')
    return render_template('view-authorization-requests.html', requests=requests)


@admin_blueprint.route('/authorization-requests/view/<string:username>/')
@login_required
@requires_roles('ADMIN')
def get_authorization_request(username):
    form = AuthorizationErrorForm()
    try:
        user = User.get(username)
    except:
        flash('Database error.')
    return render_template('view-authorization-request.html', user=user, form=form)


@admin_blueprint.route('/authorization-requests/accept/<string:username>/')
@login_required
@requires_roles('ADMIN')
def accept_authorization_request(username):
    try:
        Admin.accept_authorization_request(username)
        flash('Successfully done.')
        return redirect(url_for('admin.get_authorization_requests'))
    except MongoEngineExceptions.DoesNotExist:
        flash('User not found.')
        return redirect(url_for('admin.get_authorization_requests'))
    except MongoEngineExceptions.NotUniqueError:
        flash('Multiple users returned.')
        return redirect(url_for('admin.get_authorization_requests'))
    except Exception as error:
        print('error accept: ' + str(error))
        flash('Database error.')
        return redirect(url_for('admin.get_authorization_requests'))


@admin_blueprint.route('/authorization-requests/reject/', methods=['POST'])
@login_required
@requires_roles('ADMIN')
def reject_authorization_request():
    form = AuthorizationErrorForm()
    if form.validate_on_submit():
        try:
            Admin.reject_authorization_request(request.form.get('username'), form)
            flash('Successfully done.')
            return redirect(url_for('admin.get_authorization_requests'))
        except MongoEngineExceptions.DoesNotExist:
            flash('User not found.')
            return redirect(url_for('admin.get_authorization_requests'))
        except MongoEngineExceptions.NotUniqueError:
            flash('Multiple users returned.')
            return redirect(url_for('admin.get_authorization_requests'))
        except Exception as error:
            print(error)
            flash('Database error.')
            return redirect(url_for('admin.get_authorization_requests'))


@admin_blueprint.route('/email-template/add/', methods=['GET', 'POST'])
@login_required
@requires_roles('ADMIN')
def add_email_template():
    form = EmailTemplateForm()
    if form.validate_on_submit():
        try:
            Admin.add_email_template(form.name.data, form.html.data)
            flash('successfully saved.')
        except MongoEngineExceptions.ValidationError:
            flash('error in validation.')
            return redirect(url_for('admin.add_email_template'))
    return render_template('add_email_template.html', form=form)


@admin_blueprint.route('/check-out=payments/', methods=['GET', 'POST'])
@login_required
@requires_roles('ADMIN')
def check_out_payments():
    try:
        payments = Payment.objects(status=PAYMENT_STATUS['PAID'], type=PAYMENT_TYPE['COIN_PAYMENT'],
                                   direction=PAYMENT_DIRECTION['IN'], checked_out=False, needs_refund=False, can_withdraw=True)
        for payment in payments:
            withdrawal_price = payment.total_price - payment.commission_price
            withdrawal_payment = Payment(direction=PAYMENT_DIRECTION['OUT'], type=PAYMENT_TYPE['WITHDRAWAL'],
                                         user=payment.request.seller, total_price=withdrawal_price,
                                         status=PAYMENT_STATUS['NOT_PAID'])
            try:
                withdrawal_payment.save()
            except Exception as e:
                print(e)
                flash('Some error occurred during saving withdrawal payment for payment id: {}. Error: {}'.format(
                    payment.id, e))
                return redirect(url_for('admin.view_unpaid_withdrawal_payments'))

            commission_payment = Payment(direction=PAYMENT_DIRECTION['IN'], type=PAYMENT_TYPE['COMMISSION'],
                                         total_price=payment.commission_price,
                                         status=PAYMENT_STATUS['PAID'])
            try:
                commission_payment.save()
            except Exception as e:
                print(e)
                flash('Some error occurred during saving commission payment for payment id: {}. Error: {}'.format(
                    payment.id, e))
                return redirect(url_for('admin.view_unpaid_withdrawal_payments'))

            try:
                payment.update(checked_out=True)
                # TODO: Update to mongodb 4 for multi statement transactions.
            except Exception as e:
                flash('Some error occurred during changin payment status to checked out = True for payment id: {}. Error: {}'.format(
                    payment.id, e))
    except Exception as e:
        print(e)
        abort(500)
    flash('Payments updated successfully.')
    return redirect(url_for('admin.view_unpaid_withdrawal_payments'))

@admin_blueprint.route('/view-withdrawal-payments/')
@login_required
@requires_roles('ADMIN')
def view_unpaid_withdrawal_payments():
    try:
        payments = Payment.objects(direction=PAYMENT_DIRECTION['OUT'], type=PAYMENT_TYPE['WITHDRAWAL'], status=PAYMENT_STATUS['NOT_PAID'])
        return render_template('unpaid-withdrawal-payments.html', payments=payments)
    except Exception as e:
        print(e)
        abort(500)

@admin_blueprint.route('/view-commission-payments/')
@login_required
@requires_roles('ADMIN')
def view_commission_payments():
    try:
        payments = Payment.objects(direction=PAYMENT_DIRECTION['IN'], type=PAYMENT_TYPE['COMMISSION'])
        return render_template('commission-payments.html', payments=payments)
    except Exception as e:
        print(e)
        abort(500)

@admin_blueprint.route('/view-unsetteled-payments/')
@login_required
@requires_roles('ADMIN')
def view_unsettled_payments():
    payments = Payment.get_unsettled_payments()
    return render_template('view-unsettled-payments.html', payments=payments)

@admin_blueprint.route('/view-unresolved-exceptions/')
@login_required
@requires_roles('ADMIN')
def view_unresolved_exceptions():
    exceptions = ExceptionLog.get_unresolved_exceptions()
    return render_template('unresolved-exceptions', exceptions=exceptions)

@admin_blueprint.route('/mark-as-resolved/<string:id>/')
@login_required
@requires_roles('ADMIN')
def mark_as_resolved(id):
    ExceptionLog.mark_as_resolved(id)
    flash('Successfully done.')
    return redirect('admin.view_unresolved_exceptions')

@admin_blueprint.route('/mark-as-critical/<string:id>/')
@login_required
@requires_roles('ADMIN')
def mark_as_critical(id):
    ExceptionLog.mark_as_critical(id)
    flash('Successfully done.')
    return redirect('admin.view_unresolved_exceptions')

@admin_blueprint.route('/view-not-paid-enough/')
@login_required
@requires_roles('ADMIN')
def not_paid_enough():
    pass
