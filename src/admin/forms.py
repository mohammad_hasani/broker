from flask_wtf import FlaskForm
from wtforms import BooleanField, TextAreaField, StringField

class AuthorizationErrorForm(FlaskForm):
    fname_error = BooleanField('First Name Error', id='fname_error')
    fname_desc = TextAreaField('First Name Desc', id='fname_desc')
    lname_error = BooleanField('Last Name Error', id='lname_error')
    lname_desc = TextAreaField('Last Name Desc', id='lname_desc')
    melli_code_error = BooleanField('Melli Code Error', id='melli_code_error')
    melli_code_desc = TextAreaField('Melli Code Desc', id='melli_code_desc')
    credit_card_error = BooleanField('Credit Card Error', id='credit_card_error')
    credit_card_desc = TextAreaField('Credit Card Desc', id='credit_card_desc')
    authorization_photo_error = BooleanField('Authorization Photo Error', id='authorization_photo_error')
    authorization_photo_desc = TextAreaField('Authorization Photo Desc', id='authorization_photo_desc')
    wallet_error = BooleanField('Wallet Error', id='wallet_error')
    wallet_desc = TextAreaField('Wallet Desc', id='wallet_desc')


class EmailTemplateForm(FlaskForm):
    name = StringField('Name', id='name')
    html = TextAreaField('HTML Template', id='html_template')

