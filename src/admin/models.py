from src.misc.constants import ROLES, USER_STATUS
from src.user.models import User, AuthorizationError
from src.misc.constants import ADMIN_ITEMS_PER_PAGE, AUTHORIZATION_STATUS, USER_INFO_STATUS
from src.coin.models import Coin
from src import db


class EmailTemplate(db.Document):
    name = db.StringField()
    html = db.StringField()
    meta = {'indexes': ['name']}

    @classmethod
    def get(cls, name):
        return cls.objects.get(name=name)


class Admin:
    @staticmethod
    def elevate_user(username, role):
        if not role in ROLES:
            raise ValueError('Role %s doesnt exist.') % role
        user = User.get(username)
        user.role = role
        return user.save()

    @staticmethod
    def ban_user(username):
        user = User.get(username)
        user.status = USER_STATUS['BANNED']
        return user.save()

    @staticmethod
    def unban_user(username):
        user = User.get(username)
        user.status = USER_STATUS['ACTIVE']
        return user.save()

    @classmethod
    def get_users(cls, page=1):
        offset = (page - 1) * ADMIN_ITEMS_PER_PAGE
        users = User.objects.skip(offset).limit(ADMIN_ITEMS_PER_PAGE)
        return users

    @classmethod
    def get_authorization_requests(cls, page=1):
        offset = (page - 1) * ADMIN_ITEMS_PER_PAGE
        return User.objects(authorization_status=AUTHORIZATION_STATUS['PENDING']).skip(offset).limit(
            ADMIN_ITEMS_PER_PAGE)

    @staticmethod
    def accept_authorization_request(username):
        user = User.objects.get(username=username)
        user.authorization_errors.clear()
        user.authorization_status = AUTHORIZATION_STATUS['AUTHORIZED']
        user.fname_status = USER_INFO_STATUS['ACCEPTED']
        user.lname_status = USER_INFO_STATUS['ACCEPTED']
        user.melli_code_status = USER_INFO_STATUS['ACCEPTED']
        user.authorization_photo_status = USER_INFO_STATUS['ACCEPTED']
        user.credit_cards_status = USER_INFO_STATUS['ACCEPTED']
        return user.save()

    @staticmethod
    def reject_authorization_request(username, form):
        user = User.objects.get(username=username)
        user.authorization_errors.clear()
        if form.fname_error.data == True:
            error = AuthorizationError(for_field='fname', desc=form.fname_desc.data)
            user.authorization_errors.append(error)
            user.fname_status = USER_INFO_STATUS['REJECTED']

        if form.lname_error.data == True:
            error = AuthorizationError(for_field='lname', desc=form.lname_desc.data)
            user.authorization_errors.append(error)
            user.lname_status = USER_INFO_STATUS['REJECTED']

        if form.melli_code_error.data == True:
            error = AuthorizationError(for_field='melli_code', desc=form.melli_code_desc.data)
            user.authorization_errors.append(error)
            user.melli_code_status = USER_INFO_STATUS['REJECTED']

        if form.credit_card_error.data == True:
            error = AuthorizationError(for_field='credit_cards', desc=form.credit_card_desc.data)
            user.authorization_errors.append(error)
            user.credit_cards_status = USER_INFO_STATUS['REJECTED']

        if form.authorization_photo_error.data == True:
            error = AuthorizationError(for_field='authorization_photo', desc=form.authorization_photo_desc.data)
            user.authorization_errors.append(error)
            user.authorization_photo_status = USER_INFO_STATUS['REJECTED']

        if form.wallet_error.data == True:
            error = AuthorizationError(for_field='wallets', desc=form.wallet.data)
            user.authorization_errors.append(error)
            user.wallets_status = USER_INFO_STATUS['REJECTED']

        user.authorization_status = AUTHORIZATION_STATUS['REJECTED']
        return user.save()

    @staticmethod
    def activate_coin(name):
        Coin.enable(name)

    @staticmethod
    def deavtivate_coin(name):
        Coin.disable(name)

    @staticmethod
    def add_email_template(name, html):
        template = EmailTemplate(name=name, html=html)
        return template.save()

    # TODO admin submit to paya
    @staticmethod
    def paya_submit(excel_file_address):
        pass
