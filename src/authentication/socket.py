from flask_socketio import emit
from src import socketio


# class EventDispatcher:
#     events = {}
#
#     def dispatch(self, event_name, payload):
#         self.events[event_name](payload)
#
#     def add(self, event_name, handler):
#         self.events[event_name] = handler

def event_dispatcher(event_name, payload):
    socketio.emit(event_name, payload)

@socketio.on_error()
def error_handler(e):
    print('socket io error:', e)