from flask import Blueprint

authentication_blueprint = Blueprint(
    'authentication', __name__, template_folder='templates')

from . import views
