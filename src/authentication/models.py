from werkzeug.security import generate_password_hash, check_password_hash
from src.user.models import User
from uuid import uuid4
from src.misc.constants import USER_INFO_STATUS
from src.misc.exceptions import EmailNotVerified
from src.misc.helpers import get_verification_link
from src.misc.email_notification_worker import send_async_email
import pyavagen
from config import UPLOAD_FOLDER
import os
import random
from datetime import datetime

class AuthenticationManager():
    @staticmethod
    def authenticate_user(phonenumber_or_email_or_username, password):
        user = User.get(phonenumber_or_email_or_username)
        password_hash = user.password
        if check_password_hash(password_hash, password):
            if phonenumber_or_email_or_username == user.email and user.email_status == USER_INFO_STATUS['PENDING']:
                raise EmailNotVerified(user.username)
            return user
        else:
            return False

    # TODO move email function to another place, now if some fields have problems, email sent and user can varify his/her self. if and only if no problem exist, email have to be sent
    @staticmethod
    def register_user(username, phonenumber, password, notifications, email=None):
        password_hash = generate_password_hash(password)
        avatar = pyavagen.Avatar(pyavagen.CHAR_AVATAR, size=500, string=username,
                                 color_list=pyavagen.COLOR_LIST_MATERIAL)
        avatar_path = os.path.join(UPLOAD_FOLDER, 'avatars', username)
        avatar.generate().save(avatar_path + '.png')
        if email:
            user = User(username=username, phonenumber=phonenumber,
                        password=password_hash, email=email,
                        authorization_id=''.join(random.choice('0123456789ABCDEF') for i in range(6)),
                        registration_time=datetime.utcnow(),
                        email_verification_code=uuid4().hex, email_status=USER_INFO_STATUS['PENDING'],notifications=notifications)
            link = get_verification_link(user.email_verification_code, username)
            send_async_email.delay(email, 'Verification for Securypto', link, html=link)
        else:
            user = User(username=username, phonenumber=phonenumber,
                        password=password_hash,
                        authorization_id=''.join(random.choice('0123456789ABCDEF') for i in range(6)),
                        registration_time=datetime.utcnow(),
                        email_verification_code=uuid4().hex, email_status=USER_INFO_STATUS['PENDING'],notifications=notifications)
        return user.save()