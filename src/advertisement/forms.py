from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, BooleanField, SelectField, validators, \
    SubmitField, DecimalField
from . import PRICE_MODELS
from src.misc.circular import coin_list
from src.coin.models import Coin
from src.misc.constants import PRICE_MODELS, WALLET_TYPE, COMMISSION_TYPE


def create_ad_form(user):
    class AdvertisementForm(FlaskForm):
        coin = Coin()
        coin.initialize()
        coin = SelectField(label='کوین رو انتخاب کن',
                           validators=[validators.DataRequired("یکی رو انتخاب کن.")],
                           choices=[(i['symbol'], i['symbol'] + ' - ' + i['name'].title()) for i in coin_list])
        price_model = SelectField(choices=[(i, i) for i in PRICE_MODELS])
        # TODO get from dollar price function
        dollar_price = DecimalField(label='قیمت دلار ', default=4200)
        per_unit_price = DecimalField(label='قیمت هر واحد', default=0.0)
        wallet_type = SelectField(label='نوع کیف پول', choices=[(i, j) for i, j in WALLET_TYPE.items()])
        wallet = SelectField(label='کیف پول', validators=[validators.DataRequired("یکی رو انتخاب کن.")],
                             choices=[(wallet['address'], wallet['name'] + ' - ' + wallet['address']) for wallet
                                      in
                                      user.wallets])
        # TODO add regexp to max and min amount for get just float
        min_amount = DecimalField(label='حداقل مقدار فروش')
        max_amount = DecimalField(label='حداکثر مثدار فروش')
        private = BooleanField(default=False, label="آیتم خصوصی")
        commission_type = SelectField(label="نوع پرداخت کمیسیون", choices=[(key, value) for key, value in COMMISSION_TYPE.items()])
        submit = SubmitField(default='ارسال')

    return AdvertisementForm()
