from src import db
import requests
import urllib.request
import json
import string
import urllib


# from coinmarketcap import Market


class Coin(db.Document):
    symbol = db.StringField(required=True)
    name = db.StringField(required=True)
    price = db.DecimalField(required=True, min_value=0, precision=8)
    enabled = db.BooleanField(default=True)

    @classmethod
    def get_coin_price(cls, symbol):
        return cls.objects.get(symbol=symbol).price

    def initialize(self):
        self._add_all_coins()
        # self.update_prices(s)
        Coin.save_to_db()
        self.COIN_LIST = self.get_from_db()

    @staticmethod
    def _add_all_coins():
        coins = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=2').json()
        Coin.drop_collection()
        for coin in coins:
            coin = Coin(name=coin['id'], symbol=coin['symbol'], price=coin['price_usd'])
            coin.save()

    @staticmethod
    def _update_all_coins():
        api_coins = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=2').json()
        db_coins = Coin._get_all_coins()
        for api_coin in api_coins:
            if not any(db_coin['name'] == api_coin['id'] for db_coin in db_coins):
                coin = Coin(name=api_coin['id'], symbol=api_coin['symbol'])
                coin.save()

    @staticmethod
    def _get_all_coins(enabled=None):
        if enabled == True:
            return Coin.objects(enabled=True)
        if enabled == False:
            return Coin.objects(enabled=False)
        return Coin.objects()

    @staticmethod
    def update_prices():
        Coin.COIN_LIST.clear()
        enabled_coins = Coin._get_all_coins(True)
        coins = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=2', timeout=10, stream=True,
                             verify=False).json()
        for coin in coins:
            for enabled_coin in enabled_coins:
                if coin['id'] == enabled_coin['name']:
                    Coin.append({'symbol': coin['symbol'], 'name': coin['id'], 'price': coin['price_usd']})
        return True

    @classmethod
    def save_to_db(cls):
        coins = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=2', timeout=10, stream=True,
                             verify=False).json()
        for coin in coins:
            cls.objects(symbol=coin['symbol'], name=coin['id'], enabled=True).update_one(price=coin['price_usd'],
                                                                                         upsert=True)

        return True

    @classmethod
    def get_from_db(cls):

        coins = cls.objects()
        coin_list = []
        for coin in coins:
            pr_coin = {}
            pr_coin['symbol'] = coin['symbol']
            pr_coin['name'] = coin['name']
            pr_coin['price'] = coin['price']
            coin_list.append(pr_coin)

        return coin_list

    @classmethod
    def get_from_db_by_symbol(cls, symbol):

        coin = cls.objects.get(symbol=symbol)
        return coin

    @staticmethod
    def get_dollar_price():
        # with urllib.request.urlopen("http://www.megaweb.ir/api/money") as url:
        #     data = json.loads(url.read().decode())
        #     res = data['sell_usd']['price']
        #     res = res.replace(',', '')
            return 4200.0

    @classmethod
    def enable(cls, name):
        disabled_coins = Coin._get_all_coins(False)
        if isinstance(name, list):
            for coin_name in name:
                for coin in disabled_coins:
                    if coin['name'] == coin_name and coin['enabled'] == False:
                        coin['enabled'] = True
                        coin.save()
        else:
            for coin in disabled_coins:
                if coin['name'] == name and coin['enabled'] == False:
                    coin['enabled'] = True
                    coin.save()
        cls.update_prices()

    @classmethod
    def disable(cls, name):
        disabled_coins = Coin._get_all_coins(True)
        if isinstance(name, list):
            for coin_name in name:
                for coin in disabled_coins:
                    if coin['name'] == coin_name and coin['enabled'] == True:
                        coin['enabled'] = False
                        coin.save()
        else:
            for coin in disabled_coins:
                if coin['name'] == name and coin['enabled'] == True:
                    coin['enabled'] = False
                    coin.save()
        cls.update_prices()
