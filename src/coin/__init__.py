from flask import Blueprint
coin_blueprint = Blueprint('coin', __name__, template_folder='templates')

LAST_UPDATE = 0

from . import views
from . import sockets