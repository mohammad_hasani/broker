
from src import socketio
from src.coin.models import Coin
from flask_socketio import emit
from src.cryptoapi.ethereum_api import EthApi
from src.cryptoapi.bitcoin_api import BitcoinApi

@socketio.on('GET_BALANCE')
def GET_BALANCE(data):
    if data['coin'] == 'ETH':
        try:
            wallet_info = EthApi.get_wallet_info(data['wallet'])
            emit('GET_ETH_BALANCE_RESULT', {'balance': wallet_info['balance']})
        except Exception as e:
            emit('GET_ETH_BALANCE_ERROR', {'error': str(e)})
    elif data['coin'] == 'BTC':
        try:
            wallet_balance = BitcoinApi.get_wallet_balance(data['wallet'])
            emit('GET_BTC_BALANCE_RESULT', {'balance': wallet_balance})
        except Exception as e:
            emit('GET_BTC_BALANCE_ERROR', {'error': str(e)})
            #TODO throw meaningful exceptions in bitcoin api
    else:
        print('Cant get coin.')
        
