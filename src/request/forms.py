from flask_wtf import FlaskForm
from wtforms import SelectField, validators, SubmitField, DecimalField, StringField, IntegerField
from wtforms.fields.html5 import IntegerRangeField


def create_request_form(user, ad):
    class RequestForm(FlaskForm):
        amount = DecimalField(label="مقدار رو بنویس", places=8, validators=[validators.number_range(min=ad.min_amount, max=(ad.max_amount - ad.reserved_amount))])
        wallet = SelectField(label='کیف پول', validators=[validators.DataRequired("یکی رو انتخاب کن.")],
                             choices=[(wallet['address'], wallet['name'] + ' - ' + wallet['address']) for wallet in user.wallets if wallet.coin == ad.coin])
        submit = SubmitField(default='ارسال')

    return RequestForm()

class TransactionIdForm(FlaskForm):
    tx_hash = StringField(label='Please enter tx hash', validators=[validators.InputRequired(),
                                                                    validators.length(min=5, max=512)])
    submit = SubmitField(default='Send')



class ConflictForm(FlaskForm):
    start = SubmitField(label='Start Conflict')


class FightForm(FlaskForm):
    BUYER_WON = SubmitField()
    SELLER_WON = SubmitField()
    JUDGE_REQUEST = SubmitField()


class JudgeForm(FlaskForm):
    BUYER_WON = SubmitField()
    SELLER_WON = SubmitField()


class FeedbackForm(FlaskForm):
    feedback = IntegerRangeField()
    feedback_value = StringField()
    feedback_submit = SubmitField()
