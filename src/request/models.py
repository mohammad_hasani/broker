from flask import abort
from src import db
from src.misc.constants import REQUEST_STATUS, \
    TRANSACTION_STATUS, CHAT_TYPES, CONFLICT_STATUS, \
    COMMISSION_TYPE, ENABLED_SITE_SETTING, PRICE_MODELS, CONFLICT_VOTE, JUDGE_VOTE, \
    PAYMENT_STATUS, TRANSACTION_HASH_STATUS, STATE_STATUS
from src.sitesetting.models import SiteSetting
from mongoengine import errors as MongoEngineExceptions
from src import socketio
from src.misc.exceptions import BadStatus
from src.misc.exceptions import BadStatus, NotVotedYet, NoConflict
from src.transaction.models import Transaction
from mongoengine.queryset.visitor import Q
from decimal import Decimal
from src.coin.models import Coin
from src.user.models import User

class ChatHistory(db.EmbeddedDocument):
    sender = db.StringField()
    data = db.StringField()
    date = db.DateTimeField()
    type = db.StringField(choices=CHAT_TYPES)
    reported = db.BooleanField(default=False)


class Conflict(db.EmbeddedDocument):
    starter = db.StringField()
    status = db.StringField(choices=CONFLICT_STATUS)
    seller_vote = db.StringField(choices=CONFLICT_VOTE)
    buyer_vote = db.StringField(choices=CONFLICT_VOTE)
    judge_vote = db.StringField(choices=JUDGE_VOTE)
    date = db.DateTimeField()


class TransactionHash(db.EmbeddedDocument):
    tx_hash = db.StringField()
    status = db.ListField(
        db.StringField(choices=TRANSACTION_HASH_STATUS, default=TRANSACTION_HASH_STATUS['NOT_CHECKED']))
    amount = db.DecimalField(min_value=0, precision=8)
    date = db.DateTimeField()


class Transaction(db.EmbeddedDocument):
    id = db.StringField()
    date = db.DateTimeField()
    raw_price = db.DecimalField(min_value=0, precision=8)
    total_price = db.DecimalField(min_value=0, precision=8)
    status = db.StringField(choices=TRANSACTION_STATUS)
    payments = db.ListField(db.ReferenceField('Payment'))
    seller_transaction_hashes = db.ListField(db.EmbeddedDocumentField(TransactionHash))
    transaction_hash_history = db.ListField(db.EmbeddedDocumentField(TransactionHash))
    found_transaction_hashes = db.ListField(db.EmbeddedDocumentField(TransactionHash))
    dont_check = db.BooleanField(default=False)


class Request(db.Document):
    amount = db.DecimalField(min_value=0, precision=8, force_string=True, required=True)
    coin = db.StringField(required=True)
    buyer = db.StringField(required=True)
    seller = db.StringField(required=True)
    ad_id = db.StringField(required=True)
    buyer_wallet = db.StringField(required=True)
    seller_wallet = db.StringField()
    chat_history = db.ListField(db.EmbeddedDocumentField(ChatHistory))
    date = db.DateTimeField(required=True)
    status = db.StringField(choices=REQUEST_STATUS, default=REQUEST_STATUS['PENDING'])
    transaction = db.EmbeddedDocumentField(Transaction)
    reply_time = db.DateTimeField()
    feedback = db.IntField(min=1, max=5)
    conflict = db.EmbeddedDocumentField(Conflict)
    commission_price = db.DecimalField(min_value=0, precision=8)

    def __repr__(self):
        return '<Request: %s>' % self.id

    __str__ = __repr__

    # REQUEST METHODS:
    # ------------------------------------------------------------------------------- #
    def request_notifier(self, status, user=None):
        '''handle acception or declation of a specefic request'''

        if status == 'ACCEPT':
            socketio.emit('request_accept', message='your request with request id %s accepted' % self.id,
                          room=self.buyer)
            return

        if status == 'DECLINE':
            socketio.emit('request_decline', message='your request with request id %s declined' % self.id,
                          room=self.buyer)
            return

        if status == 'CANCEL':
            socketio.emit('request_cancel', message='request is with request id %s declined.' % self.id, room=user)
            return

        raise BadStatus

    @classmethod
    def get_sent(cls, username):
        return cls.objects(buyer=username, status=REQUEST_STATUS['PENDING']).order_by('-date')

    # TODO status have to be everything but not pending
    @classmethod
    def get_archived_sent(cls, username):
        return cls.objects(buyer=username, status__ne=REQUEST_STATUS['PENDING']).order_by('-date')

    @classmethod
    def get_received(cls, username):
        return cls.objects(seller=username, status=REQUEST_STATUS['PENDING']).order_by('-date')

    # TODO status have to be everything but not pending
    @classmethod
    def get_archived_received(cls, username):
        return cls.objects(seller=username, status__ne=REQUEST_STATUS['PENDING']).order_by('-date')

    @classmethod
    def get(cls, request_id):
        return cls.objects.get(id=request_id)

    def get_role(self, username):
        # this method get role of involved username in a request(buyer or seller or not involved)
        if username == self.buyer:
            return 'buyer'
        elif username == self.seller:
            return 'seller'
        else:
            return 'not involved'

    @classmethod
    def is_owner(cls, username, request_id):
        req = cls.objects.get(id=request_id)
        return True if req.buyer == username or req.seller == username else False

    def is_user_involved(self, username):
        if username == self.buyer or username == self.seller:
            return True
        return False

    def add_chat_history(self, chat):
        self.chat_history.append(chat)
        return self.save()

    @staticmethod
    def get_price_for_amount(ad, amount):
        if ad.price_model == PRICE_MODELS['CUSTOM_DOLLAR']:
            price = Coin.get_coin_price(ad.coin)
            return ad.dollar_price * amount * price
        elif ad.price_model == PRICE_MODELS['PER_UNIT']:
            return ad.per_unit_price * amount
        else:
            raise ValueError('Price model cant be %s' % ad.price_model)

    def add_commission_to_price(self, price, ad):
        if ad.commission_type == COMMISSION_TYPE['BUYER_PAYS']:
            return int(price + self.commission_price)

        elif ad.commission_type == COMMISSION_TYPE['SELLER_PAYS']:
            return int(price)

        elif ad.commission_type == COMMISSION_TYPE['FIFTY_FIFTY']:
            return int(price + (Decimal(0.5) * self.commission_price))

    def is_seller_or_buyer(self, username):
        if username == self.seller:
            return 'seller'
        elif username == self.buyer:
            return 'buyer'
        else:
            return 'not_involved'

    def calculate_commission_price(self):
        amount = self.amount
        ad, seller = User.get_ad_for_all(self.ad_id)
        price = self.get_price_for_amount(ad, amount)
        buyer = User.get(self.buyer)
        ONE = Decimal(1)
        HALF = Decimal(0.5)
        setting = SiteSetting.objects.get(name=ENABLED_SITE_SETTING)
        commission_rate = Decimal(setting.commission_rate / 100)
        offer_discount = None
        buyer_commission = Decimal(buyer.commission_discount)
        seller_commission = Decimal(seller.commission_discount)

        if setting.offer is not None:
            offer_discount = Decimal(setting.offer.discount)

        if ad.commission_type == COMMISSION_TYPE['BUYER_PAYS']:
            if offer_discount is not None and offer_discount > buyer_commission:
                return price * (ONE - offer_discount) * commission_rate

            return price * (ONE - buyer_commission) * commission_rate

        elif ad.commission_type == COMMISSION_TYPE['SELLER_PAYS']:
            if offer_discount is not None and offer_discount > seller_commission:
                return price * (ONE - offer_discount) * commission_rate

            return price * (ONE - seller_commission) * commission_rate

        elif ad.commission_type == COMMISSION_TYPE['FIFTY_FIFTY']:
            commission_sum = Decimal(0)
            if offer_discount is not None and offer_discount > buyer.commission_discount:
                commission_sum += price * (ONE - offer_discount) * HALF * commission_rate
            else:
                commission_sum += price * (ONE - buyer_commission) * HALF * commission_rate

            if offer_discount is not None and offer_discount > seller.commission_discount:
                commission_sum += price * (ONE - offer_discount) * HALF * commission_rate
            else:
                commission_sum += price * (ONE - seller_commission) * HALF * commission_rate
            return commission_sum
    
    def check_state(self, Payment):
        # get payment
        try:
            last_payment = self.transaction.payments[-1]
            payment = Payment.get(last_payment.id)
            payment_status = payment.status

        except AttributeError or None:
            payment = False
            payment_status = False
        # get all status and every fucking thing needed for nowing state
        except IndexError:
            payment = False
            payment_status = False
        except Exception as e:
            print(e)
            abort(500)

        request_status = self.status

        try:
            tx_status = self.transaction.status
        except AttributeError or None:
            tx_status = False
        except:
            abort(500)
        try:
            conflict_status = self.conflict.status
            conflict_starter = self.conflict.starter
        except AttributeError or None:
            conflict_status = False
            conflict_starter_role = False
        except:
            abort(500)

        # reuest_ = self.status
        # reuest_status = self.status
        # reuest_status = self.status

        # return state in diffrent situations

        # buyer states
        if request_status == REQUEST_STATUS[
            'PENDING'] and not tx_status and not conflict_status and not payment_status:
            return STATE_STATUS['REQUEST_PENDING'], 'request'

        elif request_status == REQUEST_STATUS[
            'BUYER_CANCELED'] and not tx_status and not conflict_status and not payment_status:
            return 'buyer cancel before accept', 'transaction'

        elif request_status == REQUEST_STATUS[
            'ACCEPTED'] and tx_status == TRANSACTION_STATUS['PENDING_BUYER_PAY'] and not conflict_status and not payment_status:
            return STATE_STATUS['PENDING_BUYER_PAY'], 'transaction'

        elif request_status == REQUEST_STATUS['BUYER_CANCELED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_BUYER_PAY'] and not conflict_status and (
                        (not payment_status) or (payment_status == PAYMENT_STATUS['NOT_PAYED']) or (payment_status ==
                    PAYMENT_STATUS[
                        'INVALID'])):
            return 'buyer cancel before fiat payment', 'transaction'

        elif request_status == REQUEST_STATUS['BUYER_CANCELED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_SELLER_PAY'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return 'buyer cancel after fiat payment', 'transaction'

        elif request_status == REQUEST_STATUS['ACCEPTED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_SELLER_PAY'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return PAYMENT_STATUS['PENDING_SELLER_PAY'], 'transaction'

        elif request_status == REQUEST_STATUS['ACCEPTED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_SELLER_PAY'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return 'pending seller payment'

        elif request_status == REQUEST_STATUS['DONE'] and tx_status == TRANSACTION_STATUS[
            'SELLER_PAID'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return 'buyer confirmed', 'transaction'

        elif request_status == REQUEST_STATUS['BUYER_DIDNT_PAY'] and tx_status == TRANSACTION_STATUS[
            'INVALID'] and not conflict_status and payment_status == PAYMENT_STATUS['INVALID']:
            return 'buyer timeout', 'transaction'

        elif request_status == REQUEST_STATUS[
            'DECLINED'] and not tx_status and not conflict_status and not payment_status:
            return 'declined', 'transaction'

        elif request_status == REQUEST_STATUS[
            'ACCEPTED'] and not tx_status and not conflict_status and not payment_status:
            return 'accepted', 'transaction'

        # seller states
        elif request_status == REQUEST_STATUS['SELLER_CANCELED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_BUYER_PAY'] and not conflict_status and (
                        not payment_status or payment_status == PAYMENT_STATUS['NOT_PAYED'] or payment_status ==
                    PAYMENT_STATUS[
                        'INVALID']):
            return 'buyer cancel before accept', 'transaction'

        elif request_status == REQUEST_STATUS['SELLER_DIDNT_PAY'] and tx_status == TRANSACTION_STATUS[
            'PENDING_SELLER_PAY'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return 'seller  timeout', 'transaction'

        elif request_status == REQUEST_STATUS['ACCEPTED'] and tx_status == TRANSACTION_STATUS[
            'PENDING_SELLER_PAY'] and not conflict_status and payment_status == PAYMENT_STATUS['PAID']:
            return 'seller  paid', 'transaction'

        # conflict states
        elif request_status == REQUEST_STATUS['CONFLICT'] and tx_status == TRANSACTION_STATUS[
            'SELLER_PAID'] and conflict_status == CONFLICT_STATUS[
            'OPEN'] and conflict_starter_role == 'buyer' and payment_status == PAYMENT_STATUS['PAID']:
            return 'buyer conflict after seller paid', 'conflict'

        elif request_status == REQUEST_STATUS['CONFLICT'] and tx_status == TRANSACTION_STATUS[
            'SELLER_PAID'] and conflict_status == CONFLICT_STATUS[
            'OPEN'] and conflict_starter_role == 'seller' and payment_status == PAYMENT_STATUS['PAID']:
            return 'seller conflict after seller paid', 'conflict'

        elif request_status == REQUEST_STATUS['CONFLICT'] and tx_status == TRANSACTION_STATUS[
            'SELLER_PAID'] and conflict_status == CONFLICT_STATUS[
            'OPEN'] and conflict_starter_role == 'seller' and payment_status == PAYMENT_STATUS['PAID']:
            return 'seller conflict after seller paid', 'conflict'

        elif request_status == REQUEST_STATUS['CONFLICT'] and tx_status == TRANSACTION_STATUS[
            'SELLER_PAID_AFTER_TIMEOUT'] and conflict_status == CONFLICT_STATUS[
            'OPEN'] and conflict_starter_role == 'seller' and payment_status == PAYMENT_STATUS['PAID']:
            return 'seller paid after timeout', 'conflict'
            # have to let seller enter tx id after timeout

        elif request_status == REQUEST_STATUS['DONE'] and conflict_status == CONFLICT_STATUS[
            'BUYER_WON'] and payment_status == PAYMENT_STATUS['PAID']:
            return 'conflict done and buyer won', 'conflict'

        elif request_status == REQUEST_STATUS['DONE'] and conflict_status == CONFLICT_STATUS[
            'SELLER_WON'] and payment_status == PAYMENT_STATUS['PAID']:
            return 'conflict done and seller won', 'conflict'

        elif request_status == REQUEST_STATUS['CONFLICT'] and conflict_status == CONFLICT_STATUS[
            'JUDGE_REQUEST'] and payment_status == PAYMENT_STATUS['PAID']:
            return 'in judgment', 'conflict'

    # FEEDBACK METHODS
    @classmethod
    def last_NoFeedback_request(cls, username):
        req = cls.objects.get(buyer=username, status=REQUEST_STATUS['DONE'], feedback__exists=False)
        return req

    # TRANSACTION METHODS:
    # ------------------------------------------------------------------------------- #

    @classmethod
    def get_buyer_pending_transactions(cls):
        return cls.objects(transaction__status=TRANSACTION_STATUS['PENDING_BUYER_PAY'])

    @classmethod
    def get_not_checked_seller_pending_transactions(cls):
        return cls.objects(transaction__status__in=(TRANSACTION_STATUS['PENDING_SELLER_PAY'], TRANSACTION_STATUS['SELLER_TOLD_PAID']), seller_wallet__ne=None, transaction__seller_transaction_hashes__status=TRANSACTION_HASH_STATUS['NOT_CHECKED'], status=REQUEST_STATUS['ACCEPTED'])

    @classmethod
    def get_all_seller_pending_transaction_hashes(cls):
        return cls.objects(
            transaction__status__in=(TRANSACTION_STATUS['PENDING_SELLER_PAY'], TRANSACTION_STATUS['SELLER_TOLD_PAID']),
            seller_wallet__ne=None, status=REQUEST_STATUS['ACCEPTED'])

    @classmethod
    def get_seller_pending_transactions_with_seller_tx_hash(cls):
        return cls.objects(__raw__={"$and": [
            {"status": "ACCEPTED"},
            {"transaction.dont_check": False},
            {"transaction.status": { "$in": ["PENDING_SELLER_PAY", "SELLER_TOLD_PAID"]}},
            {"transaction.seller_transaction_hashes": {"$exists": True}},
            {"transaction.seller_transaction_hashes.status": {"$size": 1}},
            {"transaction.seller_transaction_hashes.status": "OK"},
        ]
        })

    @classmethod
    def get_seller_pending_transactions_with_found_tx_hash(cls):
        return cls.objects(__raw__={"$and": [
            {"status": "ACCEPTED"},
            {"transaction.dont_check": False},
            {"transaction.status": { "$in": ["PENDING_SELLER_PAY", "SELLER_TOLD_PAID"]}},
            {"transaction.found_transaction_hashes": {"$exists": True}},
            {"transaction.found_transaction_hashes.status": {"$size": 1}},
            {"transaction.found_transaction_hashes.status": "OK"},
        ]
        })

    @classmethod
    def get_seller_paid_transactions(cls):
        return cls.objects(transaction__status=TRANSACTION_STATUS['SELLER_PAID'], status=REQUEST_STATUS['ACCEPTED'])

    def find_tx_hash(self, tx_hash):
        return next((tx for tx in self.transaction.seller_transaction_hashes if tx['tx_hash'] == tx_hash))

    # PAYMENT METHOD
    # ------------------------------------------------------------------------------- #
    def mark_payments_invalid(self):
        payments = self.transaction.payments
        for payment in payments:
            if payment.status == PAYMENT_STATUS['PAID']:
                return False
            if payment.status == PAYMENT_STATUS['NOT_PAID']:
                payment.status = PAYMENT_STATUS['INVALID']
        return True

    # CONFLICT METHOD
    def is_conflict_votes_match(self):
        return self.conflict.seller_vote == self.conflict.buyer_vote

    def conflict_process(self, judgment=False):
        if judgment == True:
            print('judge')
            result = self.conflict_status_by_judge()
            if result == CONFLICT_STATUS['BUYER_WON'] or result == CONFLICT_STATUS['SELLER_WON']:
                self.status = REQUEST_STATUS['DONE']
                self.save()
            elif result == CONFLICT_STATUS['JUDGE_REQUEST']:
                return CONFLICT_STATUS['JUDGE_REQUEST']
        else:
            result = self.conflict_status()
            if result == CONFLICT_STATUS['BUYER_WON'] or result == CONFLICT_STATUS['SELLER_WON']:
                self.status = REQUEST_STATUS['DONE']
                self.save()
            elif result == CONFLICT_STATUS['JUDGE_REQUEST']:
                return CONFLICT_STATUS['JUDGE_REQUEST']

    def conflict_status_by_judge(self):
        # TODO send notification to admin for judgment


        if self.conflict.judge_vote == CONFLICT_STATUS['SELLER_WON']:
            self.conflict.status = CONFLICT_STATUS['SELLER_WON']
            return CONFLICT_STATUS['SELLER_WON']

        elif self.conflict.judge_vote == CONFLICT_STATUS['BUYER_WON']:
            self.conflict.status = CONFLICT_STATUS['BUYER_WON']
            return CONFLICT_STATUS['BUYER_WON']

    def conflict_status(self):
        # TODO send notification to admin for judgment

        if self.conflict.seller_vote == CONFLICT_STATUS['JUDGE_REQUEST'] or self.conflict.buyer_vote == CONFLICT_STATUS[
            'JUDGE_REQUEST']:
            self.conflict.status = CONFLICT_STATUS['JUDGE_REQUEST']
            self.save()
            return CONFLICT_STATUS['JUDGE_REQUEST']
        elif not self.conflict.seller_vote:
            raise NotVotedYet('SELLER')
        elif not self.conflict.buyer_vote:
            raise NotVotedYet('BUYER')
        elif self.conflict.seller_vote == self.conflict.buyer_vote:
            self.conflict.status = self.conflict.seller_vote
            self.save()
            return self.conflict.seller_vote

        elif self.conflict.seller_vote != self.conflict.buyer_vote:
            self.conflict.status = CONFLICT_STATUS['JUDGE_REQUEST']
            self.save()
            return CONFLICT_STATUS['JUDGE_REQUEST']

    def close_conflict(self, conflict_status):
        self.status = REQUEST_STATUS['DONE']
        self.conflict.status = CONFLICT_STATUS[conflict_status]
        self.save()
        return
