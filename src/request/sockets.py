from src import socketio
from flask_socketio import emit, join_room, leave_room, rooms
from flask_login import login_required, current_user
from src.misc.constants import CHAT_TYPES, ROLES, REQUEST_STATUS
from src.misc.helpers import get_iran_datetime
from datetime import datetime
from mongoengine import errors as MongoEngineExceptions
import json


@socketio.on('new_request')
def print_request(message):
    emit('append_request', message='this is new one', room=message['owner'])


from src.request.models import Request, ChatHistory
from src import chat_worker


@socketio.on('join_chat')
@login_required
def join_chat(message):
    chatroom = message['chatroom']  # same as request id
    try:
        request = Request.objects.get(id=chatroom)
    except MongoEngineExceptions.DoesNotExist:
        return emit('join_chat_declined')
    if (request.status == REQUEST_STATUS['ACCEPTED'] or request.status == REQUEST_STATUS['CONFLICT']) and \
            (current_user.role == ROLES['ADMIN'] or request.is_user_involved(current_user.username)):
        join_room(chatroom)
        emit('join_chat_accepted')
    else:
        emit('join_chat_declined')


@socketio.on('chat_message')
@login_required
def chat_message(message):
    chatroom = message['chatroom']
    if not chatroom in rooms():
        return emit('not_in_room')
    msg_type = message['msg_type']
    sender = message['sender']
    if msg_type not in CHAT_TYPES:
        return emit('incorrect_message_type')
    msg = message['msg']
    date = get_iran_datetime(datetime.utcnow(), '%H:%M')
    chat = ChatHistory(sender=sender, data=msg, date=date, type=msg_type)
    chat_worker.enqueu_chat(dict(request=chatroom, chat=chat))
    emit('chat_message', dict(msg_type=msg_type, msg=msg, date=date, sender=sender), room=chatroom)


@socketio.on('chat_history')
@login_required
def chat_history(message):
    request_id = message['room_id']
    chat_index = message['chat_index']
    skip = (chat_index - 1) * 10
    limit = 10
    til = skip + limit
    try:
        req = Request.get(request_id)
        if req.is_user_involved(current_user.username):
            req_json = json.loads(req.to_json())
            chat_history = req_json['chat_history']
            end = False
            if skip >= len(chat_history):
                return
            if til < len(chat_history):
                messages = chat_history[skip: til]
            else:
                messages = chat_history[skip:]
                end = True
            emit('batch_message', dict(messages=messages, end=end))
    except MongoEngineExceptions.DoesNotExist:
        emit('room_id_not_found')
    except Exception as e:
        print(e)


