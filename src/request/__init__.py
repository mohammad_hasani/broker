from flask import Blueprint

request_blueprint = Blueprint('request', __name__, template_folder='templates')

from . import views
from . import sockets