from flask import render_template, redirect, url_for, flash, abort, request as flask_request
from flask_login import current_user, login_required
from . import request_blueprint
from .models import Request, Transaction, Conflict, TransactionHash
from .forms import create_request_form, TransactionIdForm, ConflictForm, FightForm, JudgeForm, FeedbackForm
from datetime import datetime
from src.misc.helpers import get_iran_datetime, get_numeric_id, check_transaction_time
from mongoengine import errors as MongoEngineExceptions
from src.misc.constants import ROLES, EMAIL_NOTIFICATION, \
    TRANSACTION_STATUS, REQUEST_STATUS, PAYMENT_DIRECTION, \
    PAYMENT_TYPE, CONFLICT_STATUS, TRANSACTION_HASH_STATUS, NOTIFICATION_TYPE, STATE_STATUS, WALLET_TYPE
from src.user.models import User
from uuid import uuid4
from src.payment.models import Payment, GatewayData
from src.misc.email_notification_worker import send_async_email
from src.misc.decorators import is_user_involved_dec, just_admin,not_ad_owner
from src.misc.exceptions import NotVotedYet, BpException
import decimal
from src.misc.bp_codes import BpCodes
import json
from src.notification.models import Notification,NotificationDetail

@request_blueprint.route('/create/<string:ad_id>/', methods=['GET', 'POST'])
@not_ad_owner
@login_required
def create(ad_id):
    # Get advertisement
    try:
        ad, seller = User.get_ad_for_all(ad_id)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Create request form
    form = create_request_form(current_user, ad)

    # Form validation
    if form.validate_on_submit():

        req = Request(seller=seller.username, buyer=current_user.username, coin=ad.coin, ad_id=ad.id,
                      amount=form.amount.data, date=datetime.utcnow(), buyer_wallet=form.wallet.data,
                      seller_wallet=ad.wallet)

        commission_price = req.calculate_commission_price()
        req.commission_price = commission_price
        try:
            _request = req.save()
            seller.append_request(ad.id, req)

            # send notification
            notfication_detail = NotificationDetail(id=uuid4().hex, type=NOTIFICATION_TYPE['INFO'], url_endpoint='request.detail',
                                       args=json.dumps({'request_id': str(_request.id)}),text='یک کاربر به شما درخواست داده است.')

            # seen field is UNSEEN by default
            Notification.send_notification(seller.username, notfication_detail)

        except Exception as e:
            print(e)
            abort(500)
        if seller.has_email_notification_for(EMAIL_NOTIFICATION['NEW_REQUEST']):  # TODO Check if email is verified
            owner_email = seller.email
            # from src import email_worker
            # email_worker.enqueue_email(owner_email, 'New Request For Coin {}'.format(ad.coin), 'Some Text', 'resp')
            # Todo solve circular error problem
            send_async_email.delay(owner_email, 'New Request For Coin {}'.format(ad.coin), 'Some Text', 'resp')
        return redirect(url_for('request.sent'))
    return render_template('request/create.html', ad=ad, form=form)


@request_blueprint.route('/sent/')
@request_blueprint.route('/sent/<string:username>/')
@login_required
def sent(username=None):
    if current_user.role == ROLES['ADMIN'] and username is not None and current_user.username != username:
        _sent_requests = Request.get_sent(username)
    else:
        _sent_requests = Request.get_sent(current_user.username)
    return render_template('request/sent/index.html', sent_requests=_sent_requests)


@request_blueprint.route('/sent/archive/')
@request_blueprint.route('/sent/archive/<string:username>/')
@login_required
def sent_archived(username=None):
    if current_user.role == ROLES['ADMIN'] and username is not None and current_user.username != username:
        _sent_requests = Request.get_archived_sent(username)
    else:
        _sent_requests = Request.get_archived_sent(current_user.username)
    return render_template('request/sent/archive.html', sent_requests=_sent_requests)


@request_blueprint.route('/received/')
@request_blueprint.route('/received/<string:username>/')
@login_required
def received(username=None):
    if current_user.role == ROLES['ADMIN'] and username is not None and current_user.username != username:
        _received_requests = Request.get_received(username)
    else:
        _received_requests = Request.get_received(current_user.username)
        print(_received_requests)
    return render_template('request/received/index.html', received_requests=_received_requests)


# TODO paginate requests and get payment info for all of them.


@request_blueprint.route('/received/archive/')
@request_blueprint.route('/received/archive/<string:username>/')
@login_required
def received_archived(username=None):
    if current_user.role == ROLES['ADMIN'] and username is not None and current_user.username != username:
        _received_requests = Request.get_archived_received(username)
    else:
        _received_requests = Request.get_archived_received(current_user.username)
        print(_received_requests)
    return render_template('request/received/archive.html', received_requests=_received_requests)


@request_blueprint.route('/accept/<string:request_id>/')
@login_required
def accept(request_id):
    try:
        # Get request
        req = Request.get(request_id)
        # Get Buyer
        buyer = User.get(req.buyer)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Check request status to be valid
    if req.status != REQUEST_STATUS['PENDING']:
        flash('request status is not valid for accepting.')
        return redirect(url_for('request.detail', request_id=req.id))

    ad_id = req.ad_id
    amount = req.amount

    # Get ad and ad's owner
    ad, user = User.get_ad_for_all(ad_id)

    # Calculate price based on amount and price model
    raw_price = Request.get_price_for_amount(ad, amount)

    # Calculate price with commission added
    total_price = req.add_commission_to_price(raw_price, ad)

    # Create a transaction for request and add payment to it
    tx = Transaction(id=uuid4().hex, date=datetime.utcnow(), status=TRANSACTION_STATUS['PENDING_BUYER_PAY'],
                     raw_price=int(raw_price), total_price=int(total_price))

    # Add transaction to request
    req.transaction = tx

    # Change request status to accepted.
    req.status = REQUEST_STATUS['ACCEPTED']

    # set reply_time in request
    req.reply_time = datetime.utcnow()

    # Save changes
    try:
        req.save()

        # Reserve amount in advertisement
        User.reserve_amount(ad_id, amount)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # sum reply time as minute to user.reply_sum
    reply_minute = (req.reply_time - req.date).seconds / 60
    current_user.reply_sum += decimal.Decimal(reply_minute)

    # increase 1 to request_answered_count field in user
    current_user.request_answered_count += 1

    # check and change( if necessary ) reply_average field in user
    current_user.reply_average()

    # current_user save
    try:
        current_user.save()
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Notify buyer that request is accepted
    req.request_notifier('ACCEPT')

    flash('request accepted')
    return redirect(url_for('request.detail', request_id=req.id))


@request_blueprint.route('/decline/<string:request_id>/')
@login_required
def decline(request_id):
    # Get request
    try:
        req = Request.get(request_id)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Change request status to declined.
    req.status = REQUEST_STATUS['DECLINED']

    # set reply_time in request
    req.reply_time = datetime.utcnow()

    # Save changes
    try:
        req.save()
    except Exception as e:
        print(e)
        abort(500)

    # sum reply time as minute to user.reply_sum
    reply_minute = (req.reply_time - req.date).seconds / 60
    current_user.reply_sum += decimal.Decimal(reply_minute)

    # increase 1 to request_answered_count field in user
    current_user.request_answered_count += 1

    # check and change( if necessary ) reply_average field in user
    current_user.reply_average()

    # current_user save
    try:
        current_user.save()
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Notfiy buyer that request is declined
    req.request_notifier('DECLINE')
    return redirect(url_for('request.received'))


@request_blueprint.route('/detail/<string:request_id>/')
@login_required
def detail(request_id):
    form = TransactionIdForm()
    try:
        req = Request.get(request_id)
        can_pay = None
        if req.transaction:
            can_pay = check_transaction_time(req.transaction.date, 900)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)
    conflict_form = ConflictForm()
    fight_form = FightForm()
    judge_form = JudgeForm()
    current_state = req.check_state(Payment)
    return render_template('/request/detail.html', request=req, iran_date=get_iran_datetime,
                           conflict=conflict_form, fight=fight_form,
                           judge=judge_form,
                           current_state=current_state, can_pay=can_pay,
                           form=form)


@request_blueprint.route('/cancel/<string:request_id>/')
@login_required
def cancel(request_id):
    # Get request
    try:
        req = Request.get(request_id)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    # Check if request in a valid state for cancellation
    state = Request.check_state(Payment)
    if (state == STATE_STATUS['REQUEST_PENDING'] or state == STATE_STATUS['PENDING_BUYER_PAY']) and current_user.username == req.buyer:
            # Change request status to buyer canceled
        req.status = REQUEST_STATUS['BUYER_CANCELED']
        #TODO: Handle seller cancellation.
        # Save changes
        try:
            req.save()
        except Exception as e:
            print(e)
            abort(500)

        if req.buyer == current_user.username:
            req.request_notifier('CANCEL', req.seller)
        elif req.seller == current_user.username:
            req.request_notifier('CANCEL', req.buyer)
        elif current_user.role == ROLES['ADMIN']:
            req.request_notifier('CANCEL', req.seller)
            req.request_notifier('CANCEL', req.buyer)

        flash('Request successfully cancelled.')
        return redirect(url_for('request.detail', request_id=request_id))
    else:
        flash('Request cant be cancelled in current state.')
        return redirect(url_for('request.detail', request_id=request_id))


@request_blueprint.route('/feedback/<string:request_id>/', methods=['get', 'post'])
@login_required
@is_user_involved_dec(user=current_user)
def feedback(user, request_id):
    form = FeedbackForm()

    if form.validate_on_submit():
        try:
            # update feedback in Transaction document
            req = Request.get(request_id)
            req.feedback = form.feedback.data
            transaction_feedback_result = req.save()

            # update feedback sum and count in User document
            if transaction_feedback_result:
                user_feedback_result = User.increment_feedback(req.seller, form.feedback.data)

            # flash and redirect to home
            if user_feedback_result:
                flash('Thank you. your feedback submitted', category='feedback')
            return redirect(url_for('home.index'))
        except MongoEngineExceptions.DoesNotExist:
            return abort(404)
        except Exception as e:
            print(e)
            return abort(500)
    else:
        print(form.errors)

    return render_template('feedback.html', feedback=form)


# Conflict views
# ____________________________________
@request_blueprint.route('/conflict/<string:request_id>/')
def conflict(request_id):
    # TODO add only request buyer or seller to all routes
    try:
        req = Request.get(request_id)
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)

    req.status = REQUEST_STATUS['CONFLICT']
    #TODO: Why dont you save this?


@request_blueprint.route('/add-transaction-hash/<string:request_id>/', methods=['POST'])
@login_required
def add_transaction_hash(request_id):
    form = TransactionIdForm()
    if form.validate_on_submit():
        req = Request.get(request_id)
        tx = req.transaction
        tx.seller_transaction_hashes.append(
            TransactionHash(tx_hash=form.tx_hash.data, status=[TRANSACTION_HASH_STATUS['NOT_CHECKED']]))
        try:
            req.save()
            flash('Your transaction hash saved successfully.')
            return redirect(url_for('request.detail', request_id=request_id))
        except MongoEngineExceptions.DoesNotExist:
            abort(404)
        except Exception as e:
            print(e)
            abort(500)
    flash('Your transaction hash is not valid.')
    return redirect(url_for('request.detail', request_id=request_id))


@request_blueprint.route('/edit-transaction-hash/<string:request_id>/<string:tx_hash>/', methods=['POST'])
@login_required
def delete_transaction_hash(request_id, tx_hash):
    form = TransactionIdForm()
    if form.validate_on_submit():
        try:
            req = Request.get(request_id)
            tx = req.find_tx_hash(tx_hash)
            req.transaction.transaction_hash_history.append(tx_hash)
            req.transaction.seller_transaction_hashes.remove(tx_hash)
            tx.seller_transaction_hash = TransactionHash(tx_hash=form.tx_hash.data)
        except MongoEngineExceptions.DoesNotExist:
            flash('Request or tx hash doesnt exist.')
            abort(404)
        except Exception as e:
            print(e)
            abort(500)
        try:
            req.save()
            flash('Your transaction hash saved successfully.')
            return redirect(url_for('request.detail', request_id=request_id))
        except Exception as e:
            print(e)
            abort(500)
    flash('Your transaction hash is not valid.')
    return redirect(url_for('request.detail', request_id=request_id))


@request_blueprint.route('/test')
def test():
    from src import payment_client
    from datetime import datetime
    date_now = str(datetime.today()).split(' ')[0].replace('-', '')
    time_now = str(datetime.today()).split(' ')[1].split('.')[0].replace(':', '')
    result = payment_client.service.bpPayRequest(terminalId=217,
                                                 userName='user217',
                                                 userPassword='83642285',
                                                 orderId=10, amount=10000, localDate=date_now,
                                                 localTime=time_now,
                                                 callBackUrl='http://google.com', payerId=0)
    return str(result)


@request_blueprint.route('/conflict/judge/<string:request_id>/', methods=['post'])
@login_required
@just_admin(user=current_user) #TODO: We already have require role.
def judge(user, request_id):
    try:
        form = JudgeForm()
        for i, j in form.data.items():
            if j == True:
                req = Request.get(request_id)
                req.conflict.judge_vote = i
                flash('your vote ,submitted!')
                # save db
                req.save()

                # check conflict condition for conflict status
                result = req.conflict_process(judgment=True)

                if result == CONFLICT_STATUS['SELLER_WON']:
                    flash('Seller won')
                    req.close_conflict('SELLER_WON')

                elif result == CONFLICT_STATUS['BUYER_WON']:
                    flash('Buyer won')
                    req.close_conflict('BUYER_WON')

        return redirect(url_for('request.detail', request_id=request_id))
    except NotVotedYet as e:
        flash(str(e).title() + " not voted yet.please wait!")
        return redirect(url_for('request.detail', request_id=request_id))
    except Exception as e:
        print(e)
        abort(500)


@request_blueprint.route('/conflict/fight/<string:request_id>/', methods=['post'])
@login_required
@is_user_involved_dec(user=current_user) #TODO: Please change dec to decorator
def fight(user, request_id):
    try:
        form = FightForm()
        for i, j in form.data.items():
            if j == True:
                req = Request.get(request_id)
                starter_role = req.is_seller_or_buyer(current_user.username)

                # create conflict index dynamicly
                voter = starter_role + '_vote'

                # edit conflict db
                req.conflict[voter] = i
                flash('your vote ,submitted!')
                # save db
                req.save()

                # check conflict condition for conflict status
                result = req.conflict_process()

                if result == CONFLICT_STATUS['JUDGE_REQUEST']:
                    flash('This request sent to admin for judgment. please wait!')
                    req.send_to_admin()
                elif result == CONFLICT_STATUS['SELLER_WON']:
                    flash('Seller won')
                    req.close_conflict('SELLER_WON')

                elif result == CONFLICT_STATUS['BUYER_WON']:
                    flash('Buyer won')
                    req.close_conflict('BUYER_WON')

        return redirect(url_for('request.detail', request_id=request_id))
    except NotVotedYet as e:
        flash(str(e).title() + " not voted yet.please wait!")
        return redirect(url_for('request.detail', request_id=request_id))
    except Exception as e:
        print(e)
        abort(500)


@request_blueprint.route('/seller-confirm-coin-payment/<string:request_id>/')
@login_required
def seller_confirm_coin_payment(request_id):
    try:
        req = Request.get(request_id)
        if req.seller == current_user.username or current_user.role == ROLES['ADMIN']:
            req.update(transaction__status=TRANSACTION_STATUS['SELLER_TOLD_PAID'])
            # TODO: Send notification to buyer that seller told paid.
        else:
            flash('You are not seller of this advertisement.')
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)
    return redirect(url_for('request.detail', request_id=request_id))


@request_blueprint.route('/buyer-confirm-request-completion/<string:request_id>/')
@login_required
def buyer_confirm_request_completion(request_id):
    try:
        req = Request.get(request_id)
        if req.buyer == current_user.username or current_user.role == ROLES['ADMIN']:
            req.update(status=REQUEST_STATUS['DONE'])
            ad = User.get_ad_for_all(req.ad_id)
            if ad.wallet_type == WALLET_TYPE['EXCHANGE']:
                User.release_amount(req.ad_id, req.amount)
            # TODO: Send notification to seller that buyer confirmed.
        else:
            flash('You are not request buyer.')
    except MongoEngineExceptions.DoesNotExist:
        abort(404)
    except Exception as e:
        print(e)
        abort(500)
    return redirect(url_for('request.detail', request_id=request_id))
