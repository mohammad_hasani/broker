from flask import render_template, request
from src import app

from flask_login import current_user
from src.misc.constants import USER_INFO_STATUS
from flask import redirect, url_for, request, abort, send_file
from json import dumps
from src.request.models import Request as request_model
from mongoengine import errors as MongoengineErrors
from src import db
from src.request.models import Request
from pymongo import database
from src.notification.models import Notification


# @app.route('/manifest.json')
# def send_manifest():
#     return send_file(BASE_DIR + '/src' + url_for('static', filename='manifest.json'))
#
# @app.route('/OneSignalSDKUpdaterWorker.js')
# def send_updater():
#     return send_file(BASE_DIR + '/src' + url_for('static', filename='OneSignalSDKUpdaterWorker.js'))
#
# @app.route('/OneSignalSDKWorker.js')
# def send_worker():
#     return send_file(BASE_DIR + '/src' + url_for('static', filename='OneSignalSDKWorker.js'))
#

def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.route("/site-map")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))

    return dumps(links)


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


@app.errorhandler(400)
def bad_request(error):
    return render_template('400.html'), 400


@app.before_request
def check_reset_password_status():
    if current_user.is_authenticated:
        if current_user.reset_password_status == USER_INFO_STATUS['CHANGE_PASSWORD']:
            if not (
                                request.endpoint == 'authentication.verify_password_reset' or request.endpoint == 'user.change_password' or request.endpoint == 'authentication.logout'):
                abort(401)


@app.before_request
def force_to_feedback():
    if current_user.is_authenticated:
        if request.endpoint != 'request.feedback' and '/static/' not in request.path:
            try:
                last_NoFeedback_request = request_model.last_NoFeedback_request(current_user.username)
                return redirect(url_for('request.feedback', request_id=last_NoFeedback_request.id))
            except MongoengineErrors.DoesNotExist:
                pass


@app.after_request
def notification_finder(response):
    try:
        x = Notification.objects(username=current_user.username, detail__url_endpoint=request.endpoint,
                                 detail__args=str(request.view_args).replace("'", '"'), detail__seen='UNSEEN').update(
            set__detail__S__seen='SEEN')
        return response
    except Exception as e:
        print(str(e))
        return response

@app.route('/print/')
def test_2():

    return 'printed'
