from flask import Flask
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from flask_socketio import SocketIO
from config import UPLOAD_FOLDER
from flask_babel import Babel
import os
from flask_mail import Mail
import zeep
import pathlib
from .misc.constants import MELLAT_WSDL, BANKTEST_WSDL
from celery import Celery
from itsdangerous import JSONWebSignatureSerializer
from flask_cache import Cache


try:
    pathlib.Path(os.path.join(UPLOAD_FOLDER, 'authorization_photos')).mkdir(parents=True, exist_ok=True)
except TypeError:
    pass
try:
    pathlib.Path(os.path.join(UPLOAD_FOLDER, 'ticket_media')).mkdir(parents=True, exist_ok=True)
except TypeError:
    pass
try:
    pathlib.Path(os.path.join(UPLOAD_FOLDER, 'avatars')).mkdir(parents=True, exist_ok=True)
except TypeError:
    pass
try:
    pathlib.Path(os.path.join(UPLOAD_FOLDER, 'profile_photos')).mkdir(parents=True, exist_ok=True)
except TypeError:
    pass

# Create upload folder if does not exist.
# TODO: create a folder for each user

app = Flask(__name__)
app.config.from_object('config')
Serializer = JSONWebSignatureSerializer(app.config['SECRET_KEY'])
socketio = SocketIO(app, manage_session=True, binary=True, logger=True)  # , logger=True, engineio_logger=True
mail = Mail(app)

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

db = MongoEngine(app)

cache = Cache(app, config={'CACHE_TYPE': 'redis'})

from src.coin.models import Coin

try:
    Coin.objects.get(symbol='BTC')
except:
    coin_init = Coin()
    coin_init.initialize()

babel = Babel(app)

login_manager = LoginManager()

payment_client = zeep.Client(wsdl=BANKTEST_WSDL, strict=False)

login_manager.login_view = 'authentication.login'

login_manager.init_app(app)

from src.misc.chat_worker import ChatWorker

chat_worker = ChatWorker(app)

from src.request import request_blueprint

app.register_blueprint(request_blueprint, url_prefix='/request')

from src.home import home_blueprint

app.register_blueprint(home_blueprint)

from src.payment import payment_blueprint

app.register_blueprint(payment_blueprint, url_prefix='/payment')

from src.admin import admin_blueprint

app.register_blueprint(admin_blueprint, url_prefix='/admin')

from src.user import user_blueprint

app.register_blueprint(user_blueprint, url_prefix='/user')

from src.authorization import authorization_blueprint

app.register_blueprint(authorization_blueprint, url_prefix='/authorization')

from src.authentication import authentication_blueprint

app.register_blueprint(authentication_blueprint, url_prefix='/authentication')

from src.support import support_blueprint

app.register_blueprint(support_blueprint, url_prefix='/support')

from src.sitesetting import sitesetting_blueprint

app.register_blueprint(sitesetting_blueprint, url_prefix='/sitesetting')

from src.advertisement import advertisement_blueprint

app.register_blueprint(advertisement_blueprint, url_prefix='/advertisement')

from src.devtools import devtools_blueprint

app.register_blueprint(devtools_blueprint, url_prefix='/devtools')


from src.notification import notification_blueprint
app.register_blueprint(notification_blueprint, url_prefix='/notification')

@babel.localeselector
def get_locale():
    return 'fa'


from src.sitesetting.models import SiteSetting

s = SiteSetting()
s.initialize()



# from src.misc.payment_worker import PaymentWorker
#
# payment_worker = PaymentWorker(app)
#
# chat_worker.start_thread()
