import requests
from src.misc.constants import ETHEXPLORER_API_KEY, ETHEXPLORER_BASE_URL
from src.misc.ethexplorer_codes import EthExplorerCodes
from src.misc.exceptions import EthExplorerException,\
    NotUnique, NotFound, \
    NotEnoughConfirmations
from datetime import datetime
import dateutil.parser
from src import cache

class EthApi:
    @staticmethod
    def get_wallet_info(address):
        '''Returns balance, totalIn, totalOut, txCount'''
        payload = {'apiKey': ETHEXPLORER_API_KEY}
        result = requests.get('/'.join([ETHEXPLORER_BASE_URL, 'getAddressInfo', address]),
                              params=payload,
                              verify=True).json()

        if result.get('error'):
            if int(result['error']['code']) == EthExplorerCodes.INVALID_ADDRESS_FORMAT.value:
                raise EthExplorerException('Invalid Address Format.')
            if int(result['error']['code']) == EthExplorerCodes.INVALID_REQUEST:
                raise EthExplorerException('Invalid Request.')
            else:
                raise EthExplorerException('Unknown Exception.')
        else:
            balance = result['ETH']['balance']
            total_in = result['ETH']['totalIn']
            total_out = result['ETH']['totalOut']
            tx_count = result['countTxs']
            return dict(balance=balance, total_in=total_in, total_out=total_out, tx_count=tx_count)

    @staticmethod
    def get_transaction_info(tx_hash):
        '''Returns txHash, timestamp, blockNumber,
        confirmations, success, from, to, value, gasLimit, gasUsed'''
        payload = {'apiKey': ETHEXPLORER_API_KEY}
        result = requests.get('/'.join([ETHEXPLORER_BASE_URL, 'getTxInfo', tx_hash]),
                              params=payload,
                              verify=True).json()
        if result.get('error'):
            raise EthExplorerException(result['error']['code'])
        else:
            return result

    @staticmethod
    def get_wallet_transactions(address, limit=5):
        '''Returns a list of transactions containing following info:
        timestamp, from, to, hash, value, success'''
        payload = {'apiKey': ETHEXPLORER_API_KEY, 'limit': limit}
        result = requests.get('/'.join([ETHEXPLORER_BASE_URL, 'getAddressTransactions', address]),
                              params=payload,
                              verify=True).json()
        if not isinstance(result, list) and result.get('error'):
            if int(result['error']['code']) == EthExplorerCodes.INVALID_ADDRESS_FORMAT.value:
                raise EthExplorerException('Invalid Address Format.')
            if int(result['error']['code']) == EthExplorerCodes.INVALID_REQUEST.value:
                raise EthExplorerException('Invalid Request.')
            else:
                raise EthExplorerException('Unknown Exception.')
        else:
            return result

    @staticmethod
    def check_tx_received(from_address, to_address, amount, buyer_payment_time):
        if not isinstance(buyer_payment_time, datetime):
            buyer_payment_time = dateutil.parser.parse(buyer_payment_time)
        to_txs = EthApi.get_wallet_transactions(to_address, 10)
        tx_list = []
        for item in to_txs:
            _from = item.get('from')
            _value = item.get('value')
            _timestamp = datetime.utcfromtimestamp(int(item.get('timestamp')))
            if _from == from_address and str(_value) == "{:.3f}".format(amount) and _timestamp > buyer_payment_time:
                tx_list.append(item)
        if len(tx_list) == 1:
            tx_info = EthApi.get_transaction_info(tx_list[-1].get('hash'))
            confirmations = tx_info.get('confirmations')
            if int(confirmations) > 12:
                return tx_info
            raise NotEnoughConfirmations
        elif len(tx_list) > 1:
            # notify admin that this transaction is smelly.
            return tx_list
        else:
            raise NotFound

    @staticmethod
    def get_all_transactions_from(from_address, to_address):
        to_txs = EthApi.get_wallet_transactions(to_address, 10)
        tx_list = []
        for item in to_txs:
            _from = item.get('from')
            if _from == from_address:
                tx_list.append(item)
        return tx_list

    @staticmethod
    def get_all_transactions_from_after(from_address, to_address, after_datetime):
        if not isinstance(after_datetime, datetime):
            after_datetime = dateutil.parser.parse(after_datetime)
        to_txs = EthApi.get_wallet_transactions(to_address, 10)
        tx_list = []
        for item in to_txs:
            _from = item.get('from')
            _timestamp = datetime.utcfromtimestamp(int(item.get('timestamp')))
            if _from == from_address and _timestamp > after_datetime:
                tx_list.append(item)
        return tx_list

    @cache.cached(timeout=3600, key_prefix='etherum_fee')
    @staticmethod
    def get_current_fee():
        result = requests.get('https://www.etherchain.org/api/gasPriceOracle').json()
        standard_gas_price = int(result['standard'])
        tx_fee = standard_gas_price * 21001
        return tx_fee
    #TODO: Deduct 1 tx fee from ad max amount. for every request reserved amount should be amount + tx_fee. update reserved amounts every hour.

# try:
#     data = EthApi.get_wallet_info('0x7270b4e9de09d5dc21222d0c9937a49089a96efa')
#     for k, v in data.items():
#         print(k + ': ' + str(v))
# except EthExplorerException as e:
#     print(e)

# try:
#     data = get_wallet_transactions('0x7270b4e9de09d5dc21222d0c9937a49089a96efa', 'ETH')
#     for item in data:
#         print('//////////////////////////////TX///////////////////////')
#         for k, v in item.items():
#             print(k + ': ' + str(v))
# except EthExplorerException as e:
#     print(e)
#
# try:
#     data = EthApi.check_tx_received('0x95eff588db625357a3795ffaed9dc5d7bc827c9d',
#                                     '0x7270b4e9de09d5dc21222d0c9937a49089a96efa',
#                                     0.031,
#                                     1514017364)
#     print(data)
    # for item in data:
    #     print('//////////////////////////////TX///////////////////////')
    #     for k, v in item.items():
    #         print(k + ': ' + str(v))
# except EthExplorerException as e:
#     print(e)
# result = EthApi.get_transaction_info('0xfe8ba6f7230b812c4281ea15a9eec36647fd00b3f0062f2255241d5fda5d63f7')
# print(result)