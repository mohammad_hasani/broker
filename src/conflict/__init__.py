from flask import Blueprint

conflict_blueprint = Blueprint('conflict', __name__, template_folder='templates')

from . import views
from . import socket
