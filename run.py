from src import socketio
from src.__app__ import *

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', debug=True, port=8080)